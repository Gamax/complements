package MainApps;

import DBFacilities.RequestServer;
import GUIFacilities.ServerGUI;
import SocketFacilities.MySocket;

import java.io.IOException;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerThread implements Runnable{

    private MySocket clientSocket;
    private RequestServer requestManager;
    private ServerGUI serverGUI;

    public ServerThread(MySocket clientSocket, RequestServer requestManager, ServerGUI serverGUI){
        this.clientSocket = clientSocket;
        this.requestManager = requestManager;
        this.serverGUI = serverGUI;
    }

    public void run()
    {
        String message;
        while(true) //test
        {
            
            try {
                message = clientSocket.Receive();

                serverGUI.printLog("[Thread] RequestReceived [" + message + "]");

                message = requestManager.TraitementServer(message);
                if(message != null)
                {
                    clientSocket.Send(message);
                    serverGUI.printLog("[Thread] ResponseSent [" + message + "]");
                }
                else
                {
                    clientSocket.Send("internal error");
                    serverGUI.printLog("[Thread] ResponseSent [internal error]");
                }
            }
            catch (SocketException e){
                serverGUI.printLog("[Thread] Connection Ended");
                return;
            }
            catch (SQLException | ClassNotFoundException | ParseException | NullPointerException | NoSuchAlgorithmException | NoSuchProviderException e) 
            {
                e.printStackTrace();
                try
                {
                    clientSocket.Send("Internal error !");
                    serverGUI.printLog("[Thread] ResponseSent [Internal error !]");
                }
                catch (IOException ex) 
                {
                    ex.printStackTrace();
                    serverGUI.printLog("[Thread] Connection Ended");
                    return;
                }
            }
             catch (IOException e) {
                e.printStackTrace();
                serverGUI.printLog("[Thread] Connection Ended");
                return;
            }
        }
    }
}

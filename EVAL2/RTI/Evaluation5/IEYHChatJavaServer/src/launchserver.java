import mainapps.Server;
import GUI.ServerSetupGUI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class launchserver {

public static void main(String args[]) {

    ServerSetupGUI setupgui;
    setupgui = new ServerSetupGUI();
    setupgui.setVisible(true);
    
    while(setupgui.isVisible()){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(launchserver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Server server = new Server();
    server.runServer(setupgui.port,setupgui.udpPort,setupgui.groupAdress);
}

}

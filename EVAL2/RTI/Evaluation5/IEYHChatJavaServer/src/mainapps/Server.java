/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package mainapps;

import DBFacilities.JDBCRequests;
import DBFacilities.RequestChatServer;
import GUI.ServerGUI;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import SocketFacilities.MyUDPSocket;

import java.io.IOException;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Server {
    
    public void runServer(int port,int udpPort,String groupAddress)
    {
        
        ServerGUI servergui = new ServerGUI();
        servergui.setVisible(true);
        
        servergui.printLog("Config loaded");
        servergui.printLog("Tcp port : "+port);
        servergui.printLog("Udp multicast group : "+ groupAddress);
        servergui.printLog("Udp port : "+ udpPort);
        
        RequestChatServer requestChatServer = null;
        
        boolean coDB = false;
        
        while(!coDB) {
            try {
                requestChatServer = new RequestChatServer(groupAddress,udpPort);
                coDB = true;
            } catch (SQLException | ClassNotFoundException e) {
                servergui.printLog("Connection to database failed retrying in 5 seconds...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            } catch (NullPointerException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchProviderException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        servergui.printLog("Connection to database initialized");
        
        try {
            MyUDPSocket udpSocket = new MyUDPSocket(groupAddress, udpPort);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        servergui.printLog("UDP socket server initialized");
        
        
        MySocketServer listeningSocket;
        try {
            listeningSocket = new MySocketServer(port);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        
        servergui.printLog("TCP socket server initialized");
        
        while(true){
            MySocket clientSocket;
            try {
                clientSocket = listeningSocket.Accept();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            
            servergui.printLog("Connection accepted");
            
            while(true)
            {
                String message;
                
                try {
                    message = clientSocket.Receive();
                    
                    servergui.printLog("RequestReceived [" + message + "]");
                    
                    message = requestChatServer.TraitementServer(message);
                    clientSocket.Send(message);
                    
                    servergui.printLog("ResponseSent [" + message + "]");
                }
                catch (IOException e){
                    servergui.printLog("Connection Ended");                  
                    break;
                }
                catch (SQLException | ClassNotFoundException e)
                {
                e.printStackTrace();
                try
                {
                clientSocket.Send("Internal error !");
                servergui.printLog("ResponseSent [Internal error !]");
                }
                catch (IOException ex)
                {
                ex.printStackTrace();
                }
                }
                catch (NullPointerException ex) {
                    ex.printStackTrace();
                } 
                catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
        }
        
    }
}

//
// Created by thoma on 9/29/2018.
//

#include "SocketServer.h"
#include "Socket.h"
#include "BaseException.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>

SocketServer::SocketServer() : Socket() {

}

void SocketServer::Bind(unsigned short Port) throw (BaseException)
{
    struct hostent * infosHost;
    struct sockaddr_in socketAdress;

    char hostname[1024];
    gethostname(hostname, 1023);
    hostname[1023] = '\0';
    if((infosHost = gethostbyname(hostname))==0)
    {
        throw BaseException("Error Bind : gethostbyname failed " + to_string(errno));
    }

    //preparation sock_addr_in
    memset(&socketAdress,0, sizeof(struct sockaddr_in));
    socketAdress.sin_family = AF_INET;
    socketAdress.sin_port = htons(Port); //htons converti le nombre en format réseau
    memcpy(&socketAdress.sin_addr,infosHost->h_addr,infosHost->h_length);

    if(bind(SocketDescriptor,(struct sockaddr *)&socketAdress, sizeof(struct sockaddr_in))==-1) //association de socketAdress avec socket
    {
        throw BaseException("Error Bind : bind() failed " + to_string(errno));
    }

}

void SocketServer::Listen() throw (BaseException)
{
    if(listen(SocketDescriptor,SOMAXCONN) == -1)
    {
        throw BaseException("Error Listen : listen() failed " + to_string(errno));
    }

}

SocketServer* SocketServer::Accept() throw (BaseException)
{

    struct sockaddr_in socketAdressClient;
    socklen_t Sockaddr_in_size = sizeof(struct sockaddr_in);

    SocketServer *ConnectionSocket = new SocketServer();
    //ConnectionSocket est le socket connecté au client, le socket principal reste en listening
    if((ConnectionSocket->SocketDescriptor = accept(SocketDescriptor,(struct sockaddr*)&socketAdressClient,&Sockaddr_in_size))==-1)
    {
        throw BaseException("Error Accept : accept() failed " + to_string(errno));
    }
    return ConnectionSocket;

}

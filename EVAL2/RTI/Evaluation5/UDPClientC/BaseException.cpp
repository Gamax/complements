#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "BaseException.h"
using namespace std;

	//Constructeurs
BaseException::BaseException(void)
{
	setMsg("Erreur !");
}
BaseException::BaseException(string message)
{
	setMsg(message);
}

//Destructeur
BaseException::~BaseException() throw()
{
}

//Setters
void BaseException::setMsg(string message)
{
	msg = message ;
}
//getters
string BaseException::getMsg() const {	return msg; }

//Autre Methode
ostream& operator<<(ostream& s,const BaseException& erreur)
{
    s<<erreur.getMsg();

	return s;
}

const char *BaseException::what() const throw(){
	return msg.c_str();
}



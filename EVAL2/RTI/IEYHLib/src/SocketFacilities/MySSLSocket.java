/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketFacilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
/**
 *
 * @author Steph
 */
public class MySSLSocket implements ISocket {

    private DataInputStream dis;
    private DataOutputStream dos;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private boolean modeObj = false;
    
    private SSLSocket SSLClient ;
    private String RemoteAddress ;
    private String fichier_Keystore = "";
    private int Port ;
    private boolean flagInit = false ;
    
    
    public int init(String pswdKeyStore,String pswdKey,String keystore)
    {
        if(flagInit)
            return 1 ;
        fichier_Keystore = keystore ;
        return init(pswdKeyStore, pswdKey);
    }
    
    public int init(String pswdKeyStore,String pswdKey)
    {
        if(flagInit)
            return 1 ;
        try {
            Security.addProvider(new BouncyCastleProvider());
            //Keystore
            KeyStore srvKeys = KeyStore.getInstance("PKCS12","BC");
            char[] password_Keystore = pswdKeyStore.toCharArray();
            FileInputStream srvKF = new FileInputStream(fichier_Keystore);
            srvKeys.load(srvKF,password_Keystore);
            //Context
            SSLContext SSLCont = SSLContext.getInstance("SSLv3");
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            char[] passwd_key = pswdKey.toCharArray();
            kmf.init(srvKeys, passwd_key);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(srvKeys);
            SSLCont.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            
            //Factory
            SSLSocketFactory SSLFactory = SSLCont.getSocketFactory();
            //Socket
            SSLClient = (SSLSocket)SSLFactory.createSocket(RemoteAddress,Port);
            initFlux();
        } catch (IOException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | UnrecoverableKeyException | CertificateException e) {
            System.out.println("Exception : ");
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            return -1 ;
        }
        return 0 ;
    }        
            
            
    private void initFlux() throws IOException
    {
        if(modeObj)
        {
            oos = new ObjectOutputStream(SSLClient.getOutputStream());
            ois = new ObjectInputStream(SSLClient.getInputStream());
        }
        else
        {
            dos = new DataOutputStream(new BufferedOutputStream(SSLClient.getOutputStream()));
            dis = new DataInputStream(new BufferedInputStream(SSLClient.getInputStream()));
            
        }
    }
            
            
   public MySSLSocket(String address,int port) throws IOException
    {
        RemoteAddress = address ;
        Port = port ;
    }
    
    public MySSLSocket(String address,int port, boolean modeObj) throws IOException
    {
        RemoteAddress = address ;
        Port = port ;
        this.modeObj = modeObj;
    }
    
    MySSLSocket(SSLSocket clientSocket, boolean modeObj) throws IOException //used by the server in accept()
    {
        SSLClient = clientSocket;
        
        this.modeObj = modeObj;
    }
    
    public boolean isAlive()
    {
        return !SSLClient.isClosed() ;
    }
    
    public String ConnectedTo()
    {
        try {
            String ip = String.valueOf(SSLClient.getRemoteSocketAddress());  
            
            return ip.substring(1);
        } catch (Exception e) {
            return "unknown address";
        }
    }
    
    public String getAddress()
    {
        try {
            String ip = String.valueOf(SSLClient.getLocalAddress())+":"+String.valueOf(SSLClient.getLocalPort());
            return ip.substring(1) ;
        } catch (Exception e) {
             return "unknown address";
        }
    }
    
    public void Send(String message) throws IOException
    {
        if(modeObj)
            throw new IOException("Socket in object mode !");
        
        dos.writeUTF(message);
        dos.flush();
    }
    
    
    public String Receive() throws IOException
    {
        if(modeObj)
            throw new IOException("Socket in object mode !");
        return  dis.readUTF();
    }
    
    public void SendObj(Object obj) throws IOException
    {
        if(!modeObj)
            throw new IOException("Socket in String mode !");
        oos.writeObject(obj);
        oos.flush();
    }
    
    public Object ReceiveObj() throws IOException, ClassNotFoundException
    {
        if(!modeObj)
            throw new IOException("Socket in String mode !");
        return ois.readObject();
    }
    
    public void Close() throws IOException{
        dis.close();
        dos.close();
        ois.close();
        oos.close();
        SSLClient.close();
    }
    
}

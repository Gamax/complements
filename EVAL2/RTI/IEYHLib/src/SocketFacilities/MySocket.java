
package SocketFacilities;


import java.io.*;
import java.net.Socket;

public class MySocket implements ISocket{
    
    private Socket legacySocket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private boolean modeObj = false;
    
    public MySocket(String address,int port) throws IOException
    {
        legacySocket = new Socket(address,port); //connect cli
        dis = new DataInputStream(new BufferedInputStream(legacySocket.getInputStream()));
        dos = new DataOutputStream(new BufferedOutputStream(legacySocket.getOutputStream()));
    }
    
    public MySocket(String address,int port, boolean modeObj) throws IOException
    {
        legacySocket = new Socket(address,port); //connect cli
        
        this.modeObj = modeObj;
        
        if(modeObj)
        {
            oos = new ObjectOutputStream(legacySocket.getOutputStream());
            ois = new ObjectInputStream(legacySocket.getInputStream());
        }
        else
        {
            dos = new DataOutputStream(new BufferedOutputStream(legacySocket.getOutputStream()));
            dis = new DataInputStream(new BufferedInputStream(legacySocket.getInputStream()));
            
        }
        
    }
    
    MySocket(Socket clientSocket, boolean modeObj) throws IOException //used by the server in accept()
    {
        legacySocket = clientSocket;
        
        this.modeObj = modeObj;
        
        if(modeObj)
        {
            oos = new ObjectOutputStream(legacySocket.getOutputStream());            
            ois = new ObjectInputStream(legacySocket.getInputStream());
        }
        else
        {
            dos = new DataOutputStream(new BufferedOutputStream(legacySocket.getOutputStream()));
            dis = new DataInputStream(new BufferedInputStream(legacySocket.getInputStream()));
            
        }
    }
    
    public boolean isAlive()
    {
        return !legacySocket.isClosed() ;
    }
    
    public String ConnectedTo()
    {
        try {
            String ip = String.valueOf(legacySocket.getRemoteSocketAddress());  
            
            return ip.substring(1);
        } catch (Exception e) {
            return "unknown address";
        }
    }
    
    public String getAddress()
    {
        try {
            String ip = String.valueOf(legacySocket.getLocalAddress())+":"+String.valueOf(legacySocket.getLocalPort());
            return ip.substring(1) ;
        } catch (Exception e) {
             return "unknown address";
        }
    }
    
    public void Send(String message) throws IOException
    {
        if(modeObj)
            throw new IOException("Socket in object mode !");
        
        dos.writeUTF(message);
        dos.flush();
    }
    
    
    public String Receive() throws IOException
    {
        if(modeObj)
            throw new IOException("Socket in object mode !");
        return  dis.readUTF();
    }
    
    public void SendObj(Object obj) throws IOException
    {
        if(!modeObj)
            throw new IOException("Socket in String mode !");
        oos.writeObject(obj);
        oos.flush();
    }
    
    public Object ReceiveObj() throws IOException, ClassNotFoundException
    {
        if(!modeObj)
            throw new IOException("Socket in String mode !");
        return ois.readObject();
    }
    
    public void Close() throws IOException{
        dis.close();
        dos.close();
        ois.close();
        oos.close();
        legacySocket.close();
    }
}
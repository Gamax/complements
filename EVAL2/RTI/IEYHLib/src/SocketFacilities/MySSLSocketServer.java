/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SocketFacilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Steph
 */
public class MySSLSocketServer implements ISocket {
    
    private SSLServerSocket SSLServer ;
    
    private boolean modeObj = false ;
    private String fichier_Keystore = "";
    private int port ;
    private boolean flagInit = false ;
    
    
    public int init(String pswdKeyStore,String pswdKey,String keystore)
    {
        if(flagInit)
            return 1 ;
        fichier_Keystore = keystore ;
        return init(pswdKeyStore, pswdKey);
    }
    
    public int init(String pswdKeyStore,String pswdKey)
    {
        if(flagInit)
            return 1 ;
        try {
            Security.addProvider(new BouncyCastleProvider());
            //Keystore
            KeyStore srvKeys = KeyStore.getInstance("PKCS12","BC");
            char[] password_Keystore = pswdKeyStore.toCharArray();
            FileInputStream srvKF = new FileInputStream(fichier_Keystore);
            srvKeys.load(srvKF,password_Keystore);
            //Context
            SSLContext SSLCont = SSLContext.getInstance("SSLv3");
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            char[] passwd_key = pswdKey.toCharArray();
            kmf.init(srvKeys, passwd_key);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(srvKeys);
            SSLCont.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            //Factory
            SSLServerSocketFactory SSLFactory = SSLCont.getServerSocketFactory();
            //Socket
            SSLServer = (SSLServerSocket)SSLFactory.createServerSocket(port);
            flagInit = true ;
        } catch (IOException | KeyManagementException | KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | UnrecoverableKeyException | CertificateException e) {
            System.out.println("Exception : ");
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            return -1 ;
        }
        return 0 ;
    }
    
    
    public MySSLSocketServer() throws IOException
    {
        port = 50000 ;
    }
    
    public MySSLSocketServer(int port) throws IOException
    {
        this.port = port ;
    }

    public MySSLSocketServer(int port, String keystore) throws IOException
    {
        fichier_Keystore = keystore;
        modeObj = false;
        this.port = port ;
    }
    
    public MySSLSocketServer(int port,boolean modeObj) throws IOException
    {
        this.modeObj = modeObj;
        this.port = port ;
    }
    
    public MySSLSocketServer(int port, String keystore, boolean modeObj) throws IOException
    {
        fichier_Keystore = keystore ;
        this.modeObj = modeObj;
        this.port = port ;
    }

    
    
    public MySSLSocket Accept() throws IOException
    {
        SSLSocket socket = (SSLSocket)SSLServer.accept();
        System.out.println("NOT HERE !");
        return new MySSLSocket(socket,modeObj);
    }

    public void Close() throws IOException{
        
        SSLServer.close();
    }
}

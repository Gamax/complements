/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HSAS;

import HSA.HSAClient;
import SocketFacilities.MySSLSocket;
import java.io.IOException;
/**
 *
 * @author Steph
 */
public class HSASClient extends HSAClient{
    
    private MySSLSocket socket ;
    
    public HSASClient()
    {
    }
    
    @Override
    public void connect(String ip, int port) throws IOException
    {
        socket = new MySSLSocket(ip, port);
        String keystore = "administrated_keystore" ;
        socket.init("Administered", "Administered",keystore);
    }
    
    @Override
    public void disconnect() throws IOException
    {
        if(socket.isAlive())
        {
            transmission("F");
            socket.Close(); 
        }
        else
            System.out.println("Socket already closed");        
    }
    
    @Override
    public void transmission(String msg) throws IOException
    {
        System.out.println("Envoi de ["+msg+"]");
        socket.Send(msg);
    }
    
    @Override
    public String reception() throws IOException
    {
        return socket.Receive();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PARP;

import DBFacilities.JDBCRequestBank;
import MessageLib.Debit;
import MessageLib.Message;
import Server.Manager;
import java.sql.SQLException;

/**
 *
 * @author Steph
 */
public class PARPServer implements Manager{

    private JDBCRequestBank BDLink ; 
    
    public PARPServer(JDBCRequestBank bdlink)
    {
        BDLink = bdlink ;
    }
    
    @Override
    public Message Traitement(Message msg) {
        if(msg.getId() != 1)
            return new Message(-1,"Unknown request !");
        
        try {
            Debit debit = (Debit)msg.getContent();
            if(BDLink.checkMontant(debit.getCreditCard(), debit.getMontant()))
            {
                msg.setID(0);
                msg.setContenu(BDLink.debit(debit.getCreditCard(), debit.getMontant()));
                return msg ;
            }
            else
            {
                msg.setID(-1);
                msg.setContenu("Insufficients funds");
                return msg ;
            }
        } catch (SQLException e) {
            return new Message(-1,"Error joining the account");
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PARP;

import MessageLib.Debit;
import MessageLib.Message;
import SocketFacilities.MySSLSocket;
import java.io.IOException;

/**
 *
 * @author Steph
 */
public class PARPClient {
    private MySSLSocket socketService;
    
    public PARPClient(String ip, int port) throws IOException
    {
        System.out.println("TEST");
        socketService = new MySSLSocket(ip, port, true);
        System.out.println("TEST");
        String keystore = "card_keystore" ;
        System.out.println("TEST");
        socketService.init("Carding", "Carding",keystore);
        System.out.println("TEST");
    }
    
    public PARPClient(MySSLSocket socketService)
    {
        this.socketService = socketService;
        socketService.init("BankingKS", "Banking");
    }
    
    public Message Debit(String carte, double montant)
    {
        Message msg = new Message();
        
        try {
            msg.setID(1);
            Debit debit = new Debit(montant, carte);
            msg.setContenu(debit);
            
            return Transmission(msg);
        } 
        catch (IOException | ClassNotFoundException e) {
            msg.setID(-1);
            msg.setContenu("Socket car error !");
            return msg;
        }
    }
    
    public Message Transmission(Message msg) throws IOException, ClassNotFoundException
    {
        System.out.println("Transmission PARP : " + msg);
        socketService.SendObj(msg);
        msg = (Message) socketService.ReceiveObj();
        System.out.println("Reception : "+ msg);
        return msg;
    }
}

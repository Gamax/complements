/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DBFacilities;

//Tables :

import CryptoLib.DigestBC;
import java.sql.ResultSet;
import java.sql.SQLException;
import Formatage.LectureResultSet;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.text.ParseException;

//TABLE :
//1 : UserVillages 
//2 : UserAgences
//3 : Activites
//4 : ActiviteReservee
//5 : Reservation
//6 : Clients
//7 : Chambres

//UPDATE : 
//8 : NTable : 1 : Insert
//8 : NTable : 2 : Delete
//+ values

//SELECT : 
//0 : ALL
//1 => N : Champs predefinis

//WHERE : 
//0 : pas d'option
//1 : option


public class RequestChatServer {

    JDBCRequests jdbcRequests;
    String groupAddress;
    int udpPort;
    DigestBC digestBC;
    
    
    
    public RequestChatServer(String groupAddress,int udpPort) throws NullPointerException, SQLException, ClassNotFoundException, NoSuchAlgorithmException, NoSuchProviderException
    {
         jdbcRequests = new JDBCRequests() ;
         this.groupAddress = groupAddress;
         this.udpPort = udpPort;
         digestBC = new DigestBC();
    }
    
    //Methode serveur
    public String TraitementServer(String query) throws SQLException, NullPointerException, ClassNotFoundException, ParseException
    {
        System.out.println(query);
        if(query.isEmpty())
            return "Query empty !";
        if(query == null)
            return "Query is null !";
        
        String table = query.substring(0,1);  
        System.out.println(table);
        switch(table)
        {
            case "1" :  return TraitementLoggingReservNumber(query);
            case "2" :  return TraitementPasswordActivites(query);
            case "3" :  return TraitementPasswordReservation(query);
            case "4" :  return TraitementPasswordMateriel(query);
            default  :  return "Unknown request altogether !";
        }

    }
    
    private String TraitementLoggingReservNumber(String query) throws SQLException
    {
        if(jdbcRequests.checkReservationID(query.substring(2))){
            return "OK:"+groupAddress+"$"+udpPort;
        }
            return "NOK";
    }
    
    private String TraitementPasswordActivites(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        String username = query.substring(2,query.indexOf("$"));
        String passwordFromDB = jdbcRequests.Getpassword("UserVillages", username );
        
        if(passwordFromDB == null){
            System.out.println("PasswordServer NULL");
            return "NOK";
        }
        
        System.out.println("PasswordServer :"+passwordFromDB);
        String ids = query.substring(query.lastIndexOf("$")+1);
        digestBC.update(username);
        digestBC.update(passwordFromDB);
        digestBC.update(ids);
        String digestServer = digestBC.digest();
        String digestClient = query.substring(query.indexOf("$")+1,query.lastIndexOf("$"));
       
        
        System.out.println("ServerDigest : " + digestServer);
        System.out.println("ClientDigest : " + digestClient);
        
        if(digestBC.isEqual(digestServer, digestClient))
        {
            return "OK:"+groupAddress+"$"+udpPort;
        }
        else
        {
            return "NOK";
        }
        
    }
    
    private String TraitementPasswordReservation(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        String username = query.substring(2,query.indexOf("$"));
        String passwordFromDB = jdbcRequests.Getpassword("UserAgences", username );
        
        if(passwordFromDB == null){
            System.out.println("PasswordServer NULL");
            return "NOK";
        }
        
        System.out.println("PasswordServer :"+passwordFromDB);
        String ids = query.substring(query.lastIndexOf("$")+1);
        System.out.println("Sequence number : " + ids);
        digestBC.update(username);
        digestBC.update(passwordFromDB);
        digestBC.update(ids);
        String digestServer = digestBC.digest();
        String digestClient = query.substring(query.indexOf("$")+1,query.lastIndexOf("$"));
        
        System.out.println("ServerDigest : " + digestServer);
        System.out.println("ClientDigest : " + digestClient);
        
        if(digestBC.isEqual(digestServer, digestClient))
        {
            return "OK:"+groupAddress+"$"+udpPort;
        }
        else
        {
            return "NOK";
        }
    }
    
    //pour partie c !
    private String TraitementPasswordMateriel(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        if(jdbcRequests.CheckPassword("UserMateriel",query.substring(2,query.indexOf("$")),query.substring(query.indexOf("$")+1)))
            return "OK:"+groupAddress+"$"+udpPort;
        else
            return "NOK";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DBFacilities;

import SocketFacilities.MySocket;

import java.io.IOException;
import java.sql.SQLException;

//TABLE :
//1 : UserVillages 
//2 : UserAgences
//3 : Activites
//4 : ActiviteReservee
//5 : Reservation
//6 : Clients
//7 : Chambres

//UPDATE : 
//8 : NTable : 1 : Insert
//8 : NTable : 2 : Delete
//+ values

//SELECT : 
//0 : ALL
//1 => N : Champs predefinis

//WHERE : 
//0 : pas d'option
//1 : option

public class RequestClient {

    private MySocket socketConnection ;


    public RequestClient(String address,int port) throws IOException, NullPointerException
    {
            
        socketConnection = new MySocket(address,port);
        //FIN suppression

    }
    
    //FUCAMP
    public String getActivitesAll(String option) throws IOException
    {
        String str;
        if(option == null || option.isEmpty())
        {
             str = "3:0:0";
        }
        else
        {
            str = "3:0:1$"+option;
        }
        
        return Transmission(str);
    }
    
    public String getActivitesCategorie(String option) throws IOException
    {
        String str;
        if(option == null || option.isEmpty())
        {
             str = "3:1:0";
        }
        else
        {
            str = "3:1:1$"+option;
        }
        return Transmission(str); 
    }
    
    public String getActivitesNom(String option) throws IOException
    {
        String str ;
        if(option == null || option.isEmpty())
        {
             str = "3:2:0";
        }
        else
        {
            str = "3:2:1$"+option;
        }
        
        return Transmission(str);
    }
    
    public String getParticipants(String option) throws IOException
    {
        String str ;
        if(option == null || option.isEmpty())
        {
             str = "4:1:0";
        }
        else
        {
            str = "4:1:1$"+option;
        }
        return Transmission(str);
    }
    
    public String InscriptionAct(String id, String client) throws IOException
    {
        return Transmission("8:4:1:"+id+","+client);
    }
    
    public String DesistementAct(String id, String client) throws IOException
    {
        return Transmission("8:4:2:"+id+","+client);
    }

    public String AddClient(String Nom,String Prenom,String Address, String Nationalite,String Naissance, String Mail, String Referant) throws IOException {
        return Transmission("8:7:0:"+Nom+","+Prenom+","+Address+","+Nationalite+","+Naissance+","+Mail+","+Referant);
    }
    
    
    //ROMP
    public String getChambreCategorie() throws IOException
    {
        return Transmission("7:1:0:CATEGORIE") ;
    }
    public String getChambreType() throws IOException
    {
        return Transmission("7:1:0:TYPE") ;
    }
    
    public String getChambreParCategorie(String categorie) throws IOException
    {
        return Transmission("7:0:1:"+categorie) ;
    }
    
    public String getChambreDate(String NOption) throws IOException
    {
        return Transmission("5:0:1:" + NOption) ; 
    }
    
    public String getClientRef(String IDReservation) throws IOException
    {
        return Transmission("5:0:2:" + IDReservation);
    }
    
    public String getReservationPossible(String Categorie, String Type, String DateDebut, String DateFin) throws IOException
    {
        return Transmission("7:1:1:"+Categorie+","+Type+","+DateDebut+","+DateFin);
    }
    
    public String addReservation( String Client, String Chambre, String DateDebut, String DateFin) throws IOException
    {
        return Transmission("8:5:1:"+Client+","+Chambre+","+DateDebut+","+DateFin);
    }
    
    public String CancelReservation(String Client, String Chambre) throws IOException
    {
        return Transmission("8:5:2:"+Client+","+Chambre);
    }
    
    public String PayReservation(String Client, String Chambre, String Credit, String Montant) throws IOException
    {
        return Transmission("8:5:3:"+Client+","+Chambre+","+Credit+","+Montant);
    }
    
    public String PayPanier(String Client, String credit) throws IOException
    {
        return Transmission("8:5:4:"+Client+","+credit);
    }
    
    public String getClient(String prenom, String nom) throws IOException
    {
        return Transmission("6:1:1:"+prenom+","+nom);
    }
    
    public String getClientID(String prenom, String nom) throws IOException
    {
        return Transmission("6:1:2:"+prenom+","+nom);
    }
    
    //Methodes Both clients
    public String CheckPassword(String client, String user, String password) throws IOException
    {
        String str;
        str = client + ":" + user + "$" + password ;
        return Transmission(str);

    }
    
    public String Transmission(String str) throws IOException
    {
        socketConnection.Send(str);
        return socketConnection.Receive();
    }
    
}

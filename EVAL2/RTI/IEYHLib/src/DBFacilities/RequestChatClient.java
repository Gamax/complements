/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DBFacilities;

import CryptoLib.DigestBC;
import SocketFacilities.MySocket;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.util.Random;

//TABLE :
//1 : UserVillages 
//2 : UserAgences
//3 : Activites
//4 : ActiviteReservee
//5 : Reservation
//6 : Clients
//7 : Chambres

//UPDATE : 
//8 : NTable : 1 : Insert
//8 : NTable : 2 : Delete
//+ values

//SELECT : 
//0 : ALL
//1 => N : Champs predefinis

//WHERE : 
//0 : pas d'option
//1 : option

public class RequestChatClient {

    private MySocket socketConnection ;
    DigestBC digestBC;
    Random generator;


    public RequestChatClient(String address,int port) throws IOException, NullPointerException, NoSuchAlgorithmException, NoSuchProviderException
    {
        generator = new Random();    
        socketConnection = new MySocket(address,port);
        digestBC = new DigestBC();

    }
    
    //Methodes Both clients
    
    //userType = 0 => reservation, 1=> villages, 2=>agence
    
    public String CheckPassword(int userType, String user, String password) throws IOException
    {
        String str;
        if(userType == 0)
        {
            str = 1 + ":" + user;
        }
        else
        {
            
            int id = generator.nextInt(1000);
            String ids = Integer.toString(id);
            
            digestBC.update(user);
            digestBC.update(password);
            digestBC.update(ids);
            String digest = digestBC.digest();
            
            str = (userType+1) + ":" + user + "$" + digest + "$" + ids ; 
        }
        return Transmission(str);

    }
    
    public String Transmission(String str) throws IOException
    {
        System.out.println("Request before send : " + str);
        socketConnection.Send(str);
        return socketConnection.Receive();
    }
    
    public void Close() throws IOException
    {
        socketConnection.Close();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DBFacilities;

//Tables :

import java.sql.ResultSet;
import java.sql.SQLException;
import Formatage.LectureResultSet;
import SPAYMAPLib.ClientSPAYMAP;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.text.ParseException;

//TABLE :
//1 : UserVillages 
//2 : UserAgences
//3 : Activites
//4 : ActiviteReservee
//5 : Reservation
//6 : Clients
//7 : Chambres

//UPDATE : 
//8 : NTable : 1 : Insert
//8 : NTable : 2 : Delete
//+ values

//SELECT : 
//0 : ALL
//1 => N : Champs predefinis

//WHERE : 
//0 : pas d'option
//1 : option


public class RequestServer {

    JDBCRequests BDLink;
    public RequestServer() throws NullPointerException, SQLException, ClassNotFoundException
    {
         BDLink = new JDBCRequests() ;
    }
    
    //Methode serveur
    public String TraitementServer(String query) throws SQLException, NullPointerException, ClassNotFoundException, ParseException, NoSuchAlgorithmException, NoSuchProviderException
    {
        System.out.println(query);
        if(query.isEmpty())
            return "Query empty !";
        if(query == null)
            return "Query is null !";
        
        String table = query.substring(0,1);  
        System.out.println(table);
        switch(table)
        {
            case "1" :  return TraitementPasswordActivites(query);
            case "2" :  return TraitementPasswordReservation(query);
            case "3" :  return TraitementActivites(query.substring(2));
            case "4" :  return TraitementActivitesReservees(query.substring(2));
            case "5" :  return TraitementReservation(query.substring(2));
            case "6" :  return TraitementClients(query.substring(2));
            case "7" :  return TraitementChambres(query.substring(2));
            case "8" :  return TraitementUpdate(query.substring(2));
            case "9" :  return TraitementPasswordPhone(query.substring(2));
            default  :  return "Unknown request altogether !";
        }

    }
    
    private String TraitementPasswordActivites(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        if(BDLink.CheckPassword("UserVillages",query.substring(2,query.indexOf("$")),query.substring(query.indexOf("$")+1)))
            return "OK$"+query.substring(2,query.indexOf("$"));
        else
            return "NOK";
    }
    
    private String TraitementPasswordPhone(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        if(BDLink.CheckPassword("UserPhone",query.substring(2,query.indexOf("$")),query.substring(query.indexOf("$")+1)))
            return "OK$"+query.substring(2,query.indexOf("$"));
        else
            return "NOK";
    }
    
    private String TraitementPasswordReservation(String query) throws SQLException, NullPointerException, ClassNotFoundException, NoSuchAlgorithmException, NoSuchProviderException
    {
        String user = query.substring(2,query.indexOf("$")) ;
        String pswd = query.substring(query.indexOf("$")+1) ;
        
        if(BDLink.CheckPassword("UserAgences",user,pswd))
        {
            return "OK$"+query.substring(2,query.indexOf("$"));
        }
        else
            return "NOK";
    }
    
    private String TraitementActivites(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        ResultSet rs ;
        String select = query.substring(0,1) ;
        String isOption = query.substring(2,3);
        String option = null;
        if(!isOption.equals("0"))
            option = query.substring(4);
        
        switch(select)
        {
            case "0" :  if(option == null)
                            rs = BDLink.GetAllActivities();
                        else
                        {
                            option = option.replaceAll(",", " AND ");
                            rs = BDLink.GetActivitesFromOptions(option);
                        }
                        break;
            case "1" :  if(option == null)
                            rs = BDLink.GetAllActivitesCategories();
                        else
                        {
                            option = option.replaceAll(",", " AND ");
                            rs = BDLink.GetActivitesCatFromOptions(option);
                        }
                        break;
            case "2" :  if(option == null)
                            rs = BDLink.GetActivitesNom();
                        else
                        {
                            option = option.replaceAll(",", " AND ");
                            rs = BDLink.GetActivitesNomFromOptions(option);
                        }
                        break;
            default  :  return  "Unknown request for activity!";
        }

        
        return LectureResultSet.formatageRSToString(rs);
    }
    
    
    private String TraitementActivitesReservees(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        ResultSet rs;
        
        String select = query.substring(0,1) ;
        String isOption = query.substring(2,3);
        String option = null;
        if(!isOption.equals("0"))
            option = query.substring(4);
        
        switch(select)
        {
            case "0" :  if(option == null)
                            rs = BDLink.GetAllActiviteReservee();
                        else
                        {
                            option = option.replaceAll(",", " AND ");
                            rs = BDLink.GetActiviteReserveeFromOption(option);
                        }
                        break;
            case "1" :  if(option == null)
                            rs = BDLink.GetActiviteReserveeNom();
                        else
                        {
                            option = option.replaceAll(",", " AND ");
                            rs = BDLink.GetActiviteReserveeNomFromOption(option);
                        }
                        break;
            default  :  return  "Unknown request reserved activity !";
        }        
        
        return LectureResultSet.formatageRSToString(rs);
    }
    
    private String TraitementReservation(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        ResultSet rs ;
        
        String select = query.substring(0,1) ;
        String isOption = query.substring(2,3);
        String option = null;
        if(!isOption.equals("0"))
            option = query.substring(4);
        
        switch(select)
        {
            case "0" :  if(isOption.equals("0"))
                            rs = BDLink.GetAllChambres();
                        else if (isOption.equals("1"))
                        {
                            rs = BDLink.getChambreDate(option);
                        }
                        else 
                            rs = BDLink.getClientRef(option);
                        break;
            default  :  return  "Unknown request reservation !";
        }        
        
        return LectureResultSet.formatageRSToString(rs);
    }
    
    private String TraitementClients(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        ResultSet rs ;
        String select = query.substring(0,1) ;
        String isOption = query.substring(2,3);
        String option = null;
        if(!isOption.equals("0"))
            option = query.substring(4);
        
        switch(select)
        {
            case "1" :  if(isOption.equals("1")) rs = BDLink.getClient(option.substring(0,option.indexOf(",")),option.substring(option.indexOf(",")+1));
                        else  rs = BDLink.getClientID(option.substring(0,option.indexOf(",")),option.substring(option.indexOf(",")+1));
                        break;
            default  :  return  "Unknown request reservation !";
        }
        return LectureResultSet.formatageRSToString(rs);
    }
    
    private String TraitementChambres(String query) throws SQLException, NullPointerException, ClassNotFoundException
    {
        ResultSet rs ;
        
       String select = query.substring(0,1) ;
        String isOption = query.substring(2,3);
        String option = null;
        if(!isOption.equals("0"))
            option = query.substring(4);
        
        switch(select)
        {
            case "0" :  if(option == null)
                            rs = BDLink.GetAllChambres();
                        else
                        {
                            rs = BDLink.getChambreCategorie(option);
                        }
                        break;
            case "1" : if(isOption.equals("0"))
                        {   
                            if(query.substring(4).equals("CATEGORIE"))
                                rs = BDLink.getCategorieChambre();
                            else
                                rs = BDLink.getTypeChambre();
                        }
                        else
                        {
                            String tmp = query.substring(4);
                            String[] str = tmp.split(",");
                            rs = BDLink.getReservationPossible(str[0],str[1],str[2],str[3]);  
                        }
                        break ;
            default  :  return  "Unknown request room !";
        }     
        return LectureResultSet.formatageRSToString(rs);
    }
    
    private String TraitementUpdate(String query) throws SQLException, NullPointerException, ClassNotFoundException, ParseException
    {
        System.out.println(query);
        String table = query.substring(0,1) ;
        String action = query.substring(2,3) ;
        
        switch(table)
        {
            case "4" :  String ID =  query.substring(4,query.indexOf(",")) ;
                        String nom =  query.substring(query.indexOf(",")+1) ;
                        if(action.equals("1"))
                        {
                            if(!BDLink.GetClientName(nom).next())
                                return nom + " n'est pas valide";
                            if(BDLink.GetActiviteReserveeNomClient(ID,nom).next())
                                return nom + " est deja inscrit";

                            return BDLink.InsciptionActivite(ID, nom);
                        }
                        else
                        {
                            if(!BDLink.GetActiviteReserveeNomClient(ID,nom).next())
                                return nom + " n'est pas inscrit a l'activite";
                            return BDLink.DesistementActivite(ID, nom);
                        }
            case "5" :  if(action.equals("1"))
                        {
                            String tmp = query.substring(4);
                            String[] str = tmp.split(",");
                            String id = BDLink.addReservation(str[0], str[1], str[2], str[3]);
                            if(id != null)
                            {
                                ResultSet rs = BDLink.getClientByID(id);
                                if(rs.next())
                                    return rs.getNString("PRENOM") + " " + rs.getNString("NOM") + " a bien été inscrit(e)";
                                else
                                    return "client pas trouvé !";
                            }
                            else
                                return "impossible de réserver cette chambre !";
                        }
                        else if(action.equals("2"))
                        {
                            String tmp = query.substring(4);
                            String[] str = tmp.split(",");
                            return BDLink.CancelReservation(str[0], str[1]);
                        }
                        else if(action.equals("3"))
                        {
                            String tmp = query.substring(4);
                            String[] str = tmp.split(",");
                            
                            return Payement(str[0], str[1], str[2], str[3]);
                        }
                        else if(action.equals("4"))
                        {
                            String tmp = query.substring(4);
                            String[] str = tmp.split(",");
                            
                            return PayementPanier(str[0], str[1]);
                        }
            case "7" : String tmp = query.substring(4);
                        String[] str = tmp.split(",");
                        return BDLink.addClient(str[0],str[1],str[2],str[3],str[4],str[5],str[6]);
            default  :  return  "Unknown request update !";           
        }
    }
    
    private String Payement(String Reservation, String Client, String Credit, String Montant) 
    {
        System.out.println("RESERVATION : " + Reservation);
        System.out.println("CREDIT : " + Credit);
        System.out.println("MONTANT : " + Montant);
        
        ClientSPAYMAP PAYLink = new ClientSPAYMAP("../Evaluation6/keystore/AppPay.p12") ;
        
        try {
            
            PAYLink.authenticate("WEBAgent", "P@ssw0Rd");
            PAYLink.handshake();
            return PAYLink.payer(Reservation, Credit, Montant);
        } catch (Exception ex) {
            System.out.println("ERREUR : " + ex.getMessage());
            return ex.getMessage();
        }
    }
    
    private String PayementPanier(String Client, String Credit)
    {
        System.out.println("CLIENT : " + Client);
        System.out.println("CREDIT : " + Credit);
        
        ClientSPAYMAP PAYLink = new ClientSPAYMAP("../Evaluation6/keystore/AppPay.p12") ;
        
        try {
            PAYLink.authenticate("WEBAgent", "P@ssw0Rd");
            PAYLink.handshake();
            return PAYLink.payerPanier(Client, Credit);
        } catch (Exception ex) {
            System.out.println("ERREUR : " + ex.getMessage());
            return ex.getMessage();
        }
    }
}




package DBFacilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class JDBCInit {
     private Connection connection;
    
    public static void LoadOracleDriver() throws ClassNotFoundException
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
    }
    
    public static void LoadMySQLDriver() throws ClassNotFoundException
    {
        Class.forName("com.mysql.jdbc.Driver");
    }
    
    public void ConnectMySQL(String address, String port,String database,String user,String password) throws SQLException
    {
        connection = DriverManager.getConnection("jdbc:mysql://"+address+":"+port+"/"+database+"?serverTimezone=UTC",user,password); //todo not sure
    }
    
    public void ConnectOracle(String address, String port,String service,String user,String password) throws SQLException
    {
        
            connection = DriverManager.getConnection("jdbc:oracle:thin:@//"+address+":"+port+"/"+service,user,password); 
        
    }
    
    synchronized public ResultSet ExecuteQuery(String query) throws SQLException
    {
        Statement instruction = connection.createStatement(); 
        return instruction.executeQuery(query);

    }
    
    synchronized public int ExecuteUpdate(String query) throws SQLException
    {
        Statement instruction = connection.createStatement(); 
        return instruction.executeUpdate(query);
    }
}
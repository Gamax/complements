/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBFacilities;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Steph
 */
public class JDBCRequestBank {
    
    
     private JDBCInit BDLink ;
    
     public JDBCRequestBank () throws NullPointerException, SQLException, ClassNotFoundException
    {
        JDBCInit.LoadOracleDriver();
        BDLink = new JDBCInit();
        BDLink.ConnectOracle("localhost", "1521", "orcl", "bank", "oracle");
    }
    
    public JDBCRequestBank (String user, String password) throws NullPointerException, SQLException, ClassNotFoundException
    {
        JDBCInit.LoadOracleDriver();
        BDLink = new JDBCInit();
        BDLink.ConnectOracle("localhost", "1521", "orcl", user, password);
    }
    
    public boolean checkMontant(String carte, double montant) throws SQLException
    {
        ResultSet rs = BDLink.ExecuteQuery("SELECT SOLDE FROM COMPTE WHERE NUMEROCOMPTE IN (SELECT COMPTE FROM CARTE WHERE NUMEROCARTE ="+carte+" ) ");
        
        if(rs.next())
        {
            double solde = rs.getDouble("SOLDE");
            return solde - montant >= 0 ;
        }
        
        return false ;
    }
    
    public String debit(String carte, double montant) throws SQLException
    {
        ResultSet rs = BDLink.ExecuteQuery("SELECT * FROM COMPTE WHERE NUMEROCOMPTE IN (SELECT COMPTE FROM CARTE WHERE NUMEROCARTE ="+carte+" ) ");
        
        if(rs.next())
        {
            double solde = rs.getDouble("SOLDE");
            String compte = rs.getNString("NUMEROCOMPTE");
            double newSolde = solde - montant ;
            
            if(BDLink.ExecuteUpdate("UPDATE COMPTE SET SOLDE = " + newSolde + " WHERE NUMEROCOMPTE = '" + compte + "'" ) > 0 )
                return "Débit de " + montant + "€ effectué";
        }
        return "Impossible de débiter !";
    }
    
}

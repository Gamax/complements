/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 *
 * @author Thomas
 */
public class Reservation implements Serializable{

    private int id;
    private String referant;
    private int chambre;
    private String dateDebut;
    private String dateFin;
    private double prix;

    public Reservation(int id, String referant, int chambre, String dateDebut, String dateFin, double prix) {
        this.id = id;
        this.referant = referant;
        this.chambre = chambre;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.prix = prix;
    }
    
    public int getId() {
        return id;
    }

    public String getReferant() {
        return referant;
    }

    public int getChambre() {
        return chambre;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public double getPrix() {
        return prix;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id=" + id + ", referant=" + referant + ", chambre=" + chambre + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", prix=" + prix + '}';
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 * ID VALUE:
 * -1 : NOK -> null
 * 0 : OK -> null
 * 1 : SynLogin -> int id
 * 2 : Login -> obj
 * 3 : Reservation -> obj
 * 4 : Pay -> obj
 * 5 : Debit -> obj
 * 6 : PayResponse -> obj
 * 7 : PayConfirmation -> hmac
 * 8 : SymKey -> tab de 2 SecretKey
 * 9 : PubKey -> obj PublicKey
 * ***************************** *
 *  SYNCHRONISATION AFFICHAGE :  *
 * ***************************** *
 * 10 : getReservations
 * 11 : getReservationsPar
 * 12 : PayPanier
 * 
 */
public class Message implements Serializable{
    private int id;
    private Object content;
    private byte [] signature;

    

    public Message() {
    }
    
    public void setID(int value)
    {
        id = value ; 
    }
    
    public void setContenu(Object value)
    {
        content = value ;
    }
    
    public int getId() {
        return id;
    }

    public Object getContent() {
        return content;
    }
    
    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public byte[] getSignature() {
        return signature;
    }

    public Message(int id, Object content) {
        this.id = id;
        this.content = content;
    }

    public Message(int id, Object content, byte[] signature) {
        this.id = id;
        this.content = content;
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "Message{" + "id=" + id + ", content=" + content + ", signOrHmac=" + signature + '}';
    }
    
}

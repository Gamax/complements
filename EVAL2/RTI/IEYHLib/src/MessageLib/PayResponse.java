/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 *
 * @author Thomas
 */
public class PayResponse implements Serializable{
    private double soldeRestant;
    private int numeroTransac;

    public PayResponse(double soldeRestant, int numeroTransac) {
        this.soldeRestant = soldeRestant;
        this.numeroTransac = numeroTransac;
    }
    
    public PayResponse()
    {
        
    }
    
    public void setSolde(double value)
    {
        soldeRestant = value ;
    }
    
    public void setNum(int value)
    {
        numeroTransac = value ;
    }

    public double getSoldeRestant() {
        return soldeRestant;
    }

    public int getNumeroTransac() {
        return numeroTransac;
    }

    @Override
    public String toString() {
        return "PayResponse{" + "soldeRestant=" + soldeRestant + ", numeroTransac=" + numeroTransac + '}';
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MessageLib;

import java.io.Serializable;

/**
 *
 * @author Steph
 */
public class Client implements Serializable{
    private String Nom ;
    private String Prenom ;

    public Client() {
    }
    
    public Client(String prenom, String nom)
    {
        Nom = nom ; 
        Prenom = prenom ;
    }
    
    public Client(Client original)
    {
        Nom = original.getNom();
        Prenom = original.getPrenom();
    }
    
    public void setNom(String value)
    {
        Nom = value ;
    }
    
    public void setPrenom(String value)
    {
       Prenom = value ;
    }
    
    public String getNom()
    {
        return Nom ;
    }
    
    public String getPrenom()
    {
        return Prenom ;
    }

    @Override
    public String toString() {
        return "Client{" + "Nom=" + Nom + ", Prenom=" + Prenom + '}';
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CCAPLib;

import CryptoLib.CryptoManager;
import DBFacilities.JDBCRequestCard;
import MessageLib.Debit;
import MessageLib.Message;
import PARP.PARPClient;
import Server.Manager ;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SealedObject;
/**
 *
 * @author Steph
 */
public class ServerCCAP implements Manager{
    
    private JDBCRequestCard BDLink ;
    private CryptoManager cryptoManager;
    private PARPClient BankLink ;
    
    public ServerCCAP(JDBCRequestCard BDLink) throws NullPointerException, SQLException, ClassNotFoundException, IOException
    {
        this.BDLink = BDLink;
        BankLink = new PARPClient("0.0.0.0", 50115);
        try {
        cryptoManager = new CryptoManager("../../Evaluation6/keystore/ServCard.p12");
        } catch (Exception e) {
            System.out.println("Exception : " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
        
        System.out.println("CCAP Key used : " + cryptoManager.getCCAPKeyName("servcard") + " / " + cryptoManager.getCCAPKeyName("servpay"));
    
    }
    
    @Override
    public Message Traitement(Message msg) {
        if(msg.getId() == 5)
        {
                
            try {
                Debit debit = (Debit) cryptoManager.asymDecryptKeystore(cryptoManager.getCCAPKeyName("servcard"), (SealedObject) msg.getContent());
                
                if(!cryptoManager.verifySignatureEmploye(cryptoManager.getCCAPKeyName("servpay"), debit.toString(), msg.getSignature()))
                {
                    msg.setID(-1);
                        msg.setContenu("Invalid Signature");
                        msg.setSignature(null);
                        return msg ;
                }
                
                msg.setSignature(null);
                
                if(BDLink.checkCard(debit.getCreditCard()))
                {
                    /*if(BDLink.checkMontant(debit.getCreditCard(), debit.getMontant()))
                    {
                        msg.setID(0);
                        msg.setContenu(BDLink.debit(debit.getCreditCard(), debit.getMontant()));
                        return msg ;
                    }
                    else
                    {
                        msg.setID(-1);
                        msg.setContenu("Insufficients funds");
                        return msg ;
                    }*/
                    
                    return BankLink.Debit(debit.getCreditCard(), debit.getMontant());
                }
                else
                {
                    msg.setID(-1);
                    msg.setContenu("Invalid card number");
                    return msg ;
                }
            } catch (Exception e) {
                Logger.getLogger(ServerCCAP.class.getName()).log(Level.SEVERE, null, e);
                msg.setID(-1);
                msg.setContenu(e.getCause() + " : " + e.getMessage());
                return msg ;
            }
            
        }
        else
        {
            msg.setID(-1);
            msg.setContenu("Unknown request");
            return msg ;
        }
    }
}

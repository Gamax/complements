/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package CryptoLib;

import DBFacilities.JDBCRequests;
import SocketFacilities.MySocket;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import MessageLib.*;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 * @author Thomas
 */
public class CryptoManager {
    private String codeProvider;
    private KeyStore keyStore;
    Random randGene;
    
    public CryptoManager(String path) throws KeyStoreException, NoSuchProviderException, FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException{
        
        randGene = new Random();
        codeProvider = "BC";
        
        Security.addProvider(new BouncyCastleProvider());
        
        keyStore = KeyStore.getInstance("PKCS12", codeProvider);
        keyStore.load(new FileInputStream(path),"".toCharArray());
    }
    
    public SessionCrypto handshakeClient(MySocket serviceSocket,String user) throws CryptoManagerException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException, UnrecoverableKeyException, IOException, ClassNotFoundException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidKeyException
    {
        Message message;
        
        //Réception clé publique
        message = (Message) serviceSocket.ReceiveObj();
        if(message.getId()!=9)
            throw new CryptoManagerException("Handshake error : Invalid AsymKey Id");
        PublicKey servPublicKey = (PublicKey) message.getContent();
        
        //envoie propre clé publique
        PublicKey cliPubKey = (keyStore.getCertificate(user)).getPublicKey();
        serviceSocket.SendObj(new Message(9, cliPubKey));
        
        //4. Reception clé sym
        message = (Message) serviceSocket.ReceiveObj();
        if(message.getId()!=8)
            throw new CryptoManagerException("Handshake error : Invalid SymKey Id");
        
        PrivateKey privKey = (PrivateKey) keyStore.getKey(user, null);
        
        SecretKey[] SymKeys = new SecretKey[2];
        SymKeys = (SecretKey[]) asymDecrypt(privKey, (SealedObject) message.getContent());
        
        return new SessionCrypto(user, SymKeys[0], SymKeys[1]);
    }
    
    public SessionCrypto handshakeServer(MySocket serviceSocket,String user) throws IOException, ClassNotFoundException, CryptoManagerException, NoSuchAlgorithmException, NoSuchProviderException, InvalidCredentialsException, KeyStoreException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException
    {
        
        Message message;
        
        //envoi clé ac chiffrement asym
        //envoi clé publique
        PublicKey servPubKey = (keyStore.getCertificate("servpay")).getPublicKey();
        serviceSocket.SendObj(new Message(9, servPubKey));
        
        //Réception clé publique
        message = (Message) serviceSocket.ReceiveObj();
        if(message.getId()!=9)
            throw new CryptoManagerException("Handshake error : Invalid AsymKey Id");
        PublicKey cliPublicKey = (PublicKey) message.getContent();
        
        //Génération clé secrète
        SecretKey[] SymKeys = new SecretKey[2];
        SymKeys[0] = generateSecretKey();
        SymKeys[1] = generateSecretKey();
        
        // envoie clé via asym
       
        SealedObject objCrypted = asymCrypt(cliPublicKey, SymKeys);
        message = new Message(8, objCrypted);
        serviceSocket.SendObj(message);
        
        return new SessionCrypto(user, SymKeys[0], SymKeys[1]);
        
    }
    
    private SecretKey generateSecretKey() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        KeyGenerator clegen = KeyGenerator.getInstance("DES",codeProvider);
        clegen.init(new SecureRandom());
        return clegen.generateKey();
    }
    
    private SealedObject asymCrypt(PublicKey key,Object obj) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException
    {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding",codeProvider);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return new SealedObject((Serializable) obj, cipher);
        
    }
    
    private Object asymDecrypt(PrivateKey key,SealedObject cryptedObject) throws IOException, ClassNotFoundException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException
    {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding",codeProvider);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return  cryptedObject.getObject(cipher);
    }
    
    public SealedObject symCrypt(SecretKey key,Object obj) throws IOException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException
    {
        Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding",codeProvider);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return new SealedObject((Serializable)obj, cipher);
    }
    
    public SealedObject asymCryptKeystore(String keyname,Object obj) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, KeyStoreException
    {
        PublicKey pubKey = (keyStore.getCertificate(keyname)).getPublicKey();
        return asymCrypt(pubKey, obj);
    }
    
    public Object asymDecryptKeystore(String keyname, SealedObject cryptedObject) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, IOException, ClassNotFoundException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException
    {
        PrivateKey privKey = (PrivateKey) keyStore.getKey(keyname, null);
        return asymDecrypt(privKey, cryptedObject);
    }
    
    public Object symDecrypt(SecretKey key,SealedObject cryptedObject) throws IOException, ClassNotFoundException, IllegalBlockSizeException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding",codeProvider);
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cryptedObject.getObject(cipher);
    }
    
    public byte [] genSignature(PrivateKey key,String contentstr) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, NoSuchProviderException
    {
        Signature s = Signature.getInstance("SHA1withRSA",codeProvider);
        s.initSign(key);
        s.update(contentstr.getBytes());
        return s.sign();
    }
    
    public boolean verifySignature(PublicKey key,String contentstr, byte [] signature) throws SignatureException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException
    {
        Signature s = Signature.getInstance("SHA1withRSA",codeProvider);
        s.initVerify(key);
        s.update(contentstr.getBytes());
        return s.verify(signature);
    }
    
    //on client
    public byte[] genSignatureEmploye(String user,String contentstr) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, InvalidKeyException, SignatureException, NoSuchProviderException
    {
        PrivateKey userKey;
        userKey = (PrivateKey) keyStore.getKey(user,null);
        
        return genSignature(userKey, contentstr);
        
    }
    
    //on server
    public boolean verifySignatureEmploye(String user,String contentstr,byte [] signature) throws SignatureException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, KeyStoreException
    {
        X509Certificate userCertif = (X509Certificate) keyStore.getCertificate(user);
        PublicKey userKey = userCertif.getPublicKey();
        return verifySignature(userKey, contentstr, signature);
    }
    
    public byte [] genHMAC(SecretKey key,String user,int numTransac) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException
    {
        Mac hmac = Mac.getInstance("HMAC-MD5",codeProvider);
        hmac.init(key);
        hmac.update(user.getBytes());
        hmac.update(Integer.toString(numTransac).getBytes());
        return hmac.doFinal();
    }
    
    public boolean verifyHMAC(SecretKey key,String user,int numTransac,byte [] hmacClient) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException
    {
        Mac hmac = Mac.getInstance("HMAC-MD5", codeProvider);
        hmac.init(key);
        hmac.update(user.getBytes());
        hmac.update(Integer.toString(numTransac).getBytes());
        byte[] hmacServer = hmac.doFinal();
        
        return MessageDigest.isEqual(hmacServer, hmacClient);
    }
    
    public String getCCAPKeyName(String username){
        Calendar calendrier = Calendar.getInstance();
        int day = calendrier.get(Calendar.DAY_OF_WEEK);
        
        switch(day){
            case 1:
                return username+"sunday";
            case 2:
                return username+"monday";
            case 3:
                return username+"tuesday";
            case 4:
                return username+"wednesday";
            case 5:
                return username+"thursday";
            case 6:
                return username+"friday";
            default:
                return username+"saturday";
                
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CryptoLib;

import javax.crypto.SecretKey;

/**
 *
 * @author Thomas
 */
public class SessionCrypto {
    private String user;
    private SecretKey cryptKey;
    private SecretKey HmacKey;

    public SessionCrypto(String user, SecretKey cryptKey, SecretKey HmacKey) {
        this.user = user;
        this.cryptKey = cryptKey;
        this.HmacKey = HmacKey;
    }

    public String getUser() {
        return user;
    }

    public SecretKey getCryptKey() {
        return cryptKey;
    }

    public SecretKey getHmacKey() {
        return HmacKey;
    }

    @Override
    public String toString() {
        return "SessionCrypto{" + "user=" + user + ", cryptKey=" + cryptKey + ", HmacKey=" + HmacKey + '}';
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluation6_applicationpayement;

import GUI.GUIManager;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Steph
 */
public class Evaluation6_ApplicationPayement {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            GUIManager manager = new GUIManager();
            try {
            manager.init();
            // TODO code application logic here
            } catch (Exception e) {
                System.out.println("OUCH exception init ! " + e.getCause() + " : " + e.getMessage());
                System.exit(1);
            }
        } catch (Exception ex) {
            System.out.println("OUCH exception creation ! " + ex.getCause() + " : " + ex.getMessage());
            System.exit(1);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import config.ConfigData;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Steph
 */
public class runServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here

        ConfigData configData = new ConfigData();
        Server server = new Server();
        server.runServer(configData.getMaxThread(),configData.getSpaymapPort(),configData.getName());
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            Logger.getLogger(runServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}

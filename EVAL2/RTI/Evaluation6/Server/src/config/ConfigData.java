/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author thoma
 */
public class ConfigData {
    private String name;
    private String DNSname;
    private int adminPort;
    private int adminPortSsl;
    private int spaymapPort;
    private String ccapAddress;
    private int ccapPort;
    private boolean threadPool;
    private int maxThread;
    private String dbName;
    private String sgbdName;
    private String version;
    private String connectionString;
    private String user;
    private String password;
    private String keystore;

    public ConfigData(String name, String DNSname, int adminPort, int adminPortSsl, int spaymapPort, String ccapAddress, int ccapPort, boolean threadPool, int maxThread, String dbName, String sgbdName, String version, String connectionString, String user, String password, String keystore) {
        this.name = name;
        this.DNSname = DNSname;
        this.adminPort = adminPort;
        this.adminPortSsl = adminPortSsl;
        this.spaymapPort = spaymapPort;
        this.ccapAddress = ccapAddress;
        this.ccapPort = ccapPort;
        this.threadPool = threadPool;
        this.maxThread = maxThread;
        this.dbName = dbName;
        this.sgbdName = sgbdName;
        this.version = version;
        this.connectionString = connectionString;
        this.user = user;
        this.password = password;
        this.keystore = keystore;
    }

    public ConfigData() throws SAXException, ParserConfigurationException, IOException {
        ConfigHandlerImpl configHandlerImpl = new ConfigHandlerImpl(this);
        EntityResolver resolver = null;
        ConfigParser parser = new ConfigParser(configHandlerImpl, resolver);
        InputSource input = new InputSource("src/config/config.xml");
        parser.parse(input);
    }

    public String getName() {
        return name;
    }

    public String getDNSname() {
        return DNSname;
    }

    public int getAdminPort() {
        return adminPort;
    }

    public int getAdminPortSsl() {
        return adminPortSsl;
    }

    public int getSpaymapPort() {
        return spaymapPort;
    }

    public String getCcapAddress() {
        return ccapAddress;
    }

    public int getCcapPort() {
        return ccapPort;
    }

    public boolean isThreadPool() {
        return threadPool;
    }

    public int getMaxThread() {
        return maxThread;
    }

    public String getDbName() {
        return dbName;
    }

    public String getSgbdName() {
        return sgbdName;
    }

    public String getVersion() {
        return version;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getKeystore() {
        return keystore;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDNSname(String DNSname) {
        this.DNSname = DNSname;
    }

    public void setAdminPort(int adminPort) {
        this.adminPort = adminPort;
    }

    public void setAdminPortSsl(int adminPortSsl) {
        this.adminPortSsl = adminPortSsl;
    }

    public void setSpaymapPort(int spaymapPort) {
        this.spaymapPort = spaymapPort;
    }

    public void setCcapAddress(String ccapAddress) {
        this.ccapAddress = ccapAddress;
    }

    public void setCcapPort(int ccapPort) {
        this.ccapPort = ccapPort;
    }

    public void setThreadPool(boolean threadPool) {
        this.threadPool = threadPool;
    }

    public void setMaxThread(int maxThread) {
        this.maxThread = maxThread;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setSgbdName(String sgbdName) {
        this.sgbdName = sgbdName;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setKeystore(String keystore) {
        this.keystore = keystore;
    }

    @Override
    public String toString() {
        return "ConfigData{" + "name=" + name + ", DNSname=" + DNSname + ", adminPort=" + adminPort + ", adminPortSsl=" + adminPortSsl + ", spaymapPort=" + spaymapPort + ", ccapAddress=" + ccapAddress + ", ccapPort=" + ccapPort + ", threadPool=" + threadPool + ", maxThread=" + maxThread + ", dbName=" + dbName + ", sgbdName=" + sgbdName + ", version=" + version + ", connectionString=" + connectionString + ", user=" + user + ", password=" + password + ", keystore=" + keystore + '}';
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serveurcard;

import CCAPLib.ServerCCAP;
import DBFacilities.JDBCRequestCard;
import Server.Manager;
import SocketFacilities.ISocket;
import SocketFacilities.MySocket;
import SocketFacilities.MySocketServer;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.Server;
import server.ServerGUI;

/**
 *
 * @author Steph
 */
public class ServerCard extends Server {
    
    private JDBCRequestCard BDLinkCard ;
    
    @Override
    protected void setBDLink() throws SQLException, NullPointerException, ClassNotFoundException
    {
        BDLinkCard = new JDBCRequestCard();
    }
    
    @Override
    protected Manager setManager(ISocket socketService) throws NullPointerException, SQLException, ClassNotFoundException
    {
        try {
            return (Manager) new ServerCCAP(BDLinkCard);
        } catch (IOException ex) {
            System.exit(1);
        }
        return null ;
    }
    
    @Override
    protected void RunServer(int nbThread, ISocket Socket, ServerGUI servergui)
   {
        MySocketServer listeningSocket = (MySocketServer) Socket ;
        ExecutorService threadPool = Executors.newFixedThreadPool(nbThread);

        servergui.printLog("Thread pool initialized");
        
        try { 
            requestManager = setManager(listeningSocket);
        } catch (NullPointerException | SQLException | ClassNotFoundException ex) {
            servergui.printLog("Error setting core functionnalities, quitting in 5 seconds...");
            servergui.printLog(ex.getCause() + " : " + ex.getMessage());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex1) {
                Logger.getLogger(ServerCard.class.getName()).log(Level.SEVERE, null, ex1);
            }
            System.exit(1);
        } 
        
        while(true){
            MySocket clientSocket;
            try {
                clientSocket = listeningSocket.Accept();
            } catch (IOException e) {
                return;
            }
            try {
                ServerThread srvThread = new ServerThread(clientSocket, requestManager, servergui);
                threadPool.execute(srvThread);
                servergui.printLog("Connection accepted");
            } catch (NullPointerException ex) {
                servergui.printLog("Connection refused");
            }
            
        }
    }
    
}

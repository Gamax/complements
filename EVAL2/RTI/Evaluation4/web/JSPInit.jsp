<%-- 
    Document   : JSPInit
    Created on : Nov 28, 2018, 5:08:39 PM
    Author     : Thomas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSPInit</title>
    </head>
    
    
    
    <body>
        <% String userid = (String) session.getAttribute("userid"); %>
        Connecté en tant que <%=userid %>
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="logOut">
            <P><input type="submit" value="Log Out"></P>
        </form>
        <br>
        
        
        
        <h1>Bienvenue !</h1>
        
        <form method="POST" action="/Evaluation4/ControlServlet">
            <input type="hidden" name="requestType" value="createPanier">
            
            <% if(request.getAttribute("newUser")!=null) { %>
            <input type="hidden" name="newUser" value="on">
            <input type="hidden" name="login" value=<%=request.getAttribute("login") %>>
            <input type="hidden" name="password" value=<%=request.getAttribute("password")%>>
            <P>Nouveau Client :</P>
            <P>Nom : <input type="text" name="nom" size=20></P>
            <P>Prenom : <input type="text" name="prenom" size=20></P>
            <P>Address : <input type="text" name="address" size=20></P>
            <P>Nationalite : <input type="text" name="nationalite" size=20></P>
            <P>Naissance : <input type="text" name="naissance" size=20></P>
            <P>Mail : <input type="text" name="mail" size=20></P>
            
            <% }else{ %>
            <P>Vous êtes déjà enregistrés.</P>
            <% } %>
            
            <P><input type="submit" value="Créer panier"></P>
        </form>
            
            <% if(request.getAttribute("message") != null) { %>
        <P><%=request.getAttribute("message")%><P>
        <% } %>
    </body>
</html>

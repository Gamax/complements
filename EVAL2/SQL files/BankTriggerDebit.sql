create or replace TRIGGER Compte
    AFTER INSERT OR UPDATE OF Solde ON Compte
    FOR EACH ROW 
DECLARE 
    negativeAmount EXCEPTION ;
BEGIN
    if(:new.Solde < 0 ) THEN RAISE negativeAmount ;
    end if ;
EXCEPTION
    WHEN negativeAmount THEN RAISE_APPLICATION_ERROR (-20201, 'Not enough money on the account');
    WHEN NO_DATA_FOUND THEN RAISE ;
    WHEN OTHERS THEN RAISE ;
END ;
/
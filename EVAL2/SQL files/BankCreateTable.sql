CREATE TABLE compte 
(
    numeroCompte VARCHAR2(34) CONSTRAINT PK$Compte PRIMARY KEY,
    solde Number(10,2) CONSTRAINT NN$Compte$Solde NOT NULL,
    CONSTRAINT CK$FORMAT$NUMCOMPTE CHECK (length(numeroCompte) >= 14)
);


CREATE TABLE Carte
(
    numeroCarte NUMBER(16) CONSTRAINT PK$Carte PRIMARY KEY,
    compte VARCHAR2(16) CONSTRAINT FK$Carte$Compte REFERENCES Compte(numeroCompte),
    CONSTRAINT CK$precision$numCard$Carte CHECK (length(to_char(numeroCarte)) = 16 )
);

commit ;
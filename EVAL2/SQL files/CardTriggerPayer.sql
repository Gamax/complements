create or replace TRIGGER reservationPayee
    AFTER INSERT OR UPDATE OF montant ON ContenuFacture
    FOR EACH ROW 
DECLARE 
    prixTotal NUMBER(10,3) ;
    clientRef_ NUMBER(6,0) ;
    username_ VARCHAR2(9) ;
    exist NUMBER(6,0) ;
    negativeAmount EXCEPTION ;
    higherAmount EXCEPTION ;
    pk_exist EXCEPTION;
    PRAGMA EXCEPTION_INIT(pk_exist,-00001);
BEGIN
    SELECT PRIXNET INTO prixTotal FROM RESERVATION WHERE IDENTIFIANT = :NEW.RESERVATION ; 
    if(:new.montant < 0 ) THEN RAISE negativeAmount ;
    end if ;
    if(:new.montant > prixTotal ) THEN RAISE higherAmount ;
    end if ;
    if(:new.montant = prixTotal)
    THEN 
        UPDATE RESERVATION SET PAYE='Y' WHERE IDENTIFIANT = :NEW.Reservation ;
        SELECT REFERANT INTO clientRef_ FROM RESERVATION WHERE IDENTIFIANT = :NEW.RESERVATION ;
        SELECT LOWER(SUBSTR((SUBSTR(NOM, 1, 5)| |PRENOM), 1, 7)) AS "USERNAME" INTO username_ FROM CLIENTS WHERE ID = clientRef_ ; 
        INSERT INTO USERPHONE VALUES (username_,dbms_random.string('X', 8),clientRef_) ;
    end if ;
EXCEPTION
    WHEN negativeAmount THEN RAISE_APPLICATION_ERROR (-20201, 'amount specified cannot be negative !');
    WHEN higherAmount THEN RAISE_APPLICATION_ERROR (-20202, 'amount specified higher than amount to pay !');
    WHEN pk_exist THEN 
    BEGIN
        SELECT CLIENTREF INTO exist FROM USERPHONE WHERE CLIENTREF = clientRef_ ;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN 
            BEGIN
                INSERT INTO USERPHONE VALUES (username_,dbms_random.string('X', 8),clientRef_) ;
            EXCEPTION
                WHEN pk_exist THEN NULL;
            END ;
    END ;
    WHEN NO_DATA_FOUND THEN RAISE ;
    WHEN OTHERS THEN RAISE ;
END ;
/
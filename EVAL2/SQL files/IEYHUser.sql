create role test_role not identified;
grant alter session to test_role ;
grant create database link to test_role ;
grant create session to test_role ;
grant create procedure to test_role ;
grant create sequence to test_role ;
grant create table to test_role ;
grant create trigger to test_role ;
grant create type to test_role ;
grant create synonym to test_role ;
grant create view to test_role ;
grant create job to test_role ;
grant create materialized view to test_role ;
grant execute on sys.dbms_lock to test_role ;
grant execute on sys.owa_opt_lock to test_role ;

create user test identified by oracle account unlock;
alter user test quota unlimited on users;
grant test_role  to test ;

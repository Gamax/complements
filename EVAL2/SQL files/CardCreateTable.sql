CREATE TABLE Carte
(
    numeroCarte NUMBER(16) CONSTRAINT PK$Carte PRIMARY KEY,
    CONSTRAINT CK$precision$numCard$Carte CHECK (length(to_char(numeroCarte)) = 16 )
);

commit ;
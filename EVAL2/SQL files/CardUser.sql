create role card_role not identified;
grant alter session to card_role;
grant create database link to card_role;
grant create session to card_role;
grant create procedure to card_role;
grant create sequence to card_role;
grant create table to card_role;
grant create trigger to card_role;
grant create type to card_role;
grant create synonym to card_role;
grant create view to card_role;
grant create job to card_role;
grant create materialized view to card_role;
grant execute on sys.dbms_lock to card_role;
grant execute on sys.owa_opt_lock to card_role;

create user card identified by oracle account unlock;
alter user card quota unlimited on users;
grant card_role to card;

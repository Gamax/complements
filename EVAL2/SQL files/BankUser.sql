create role bank_role not identified;
grant alter session to bank_role;
grant create database link to bank_role;
grant create session to bank_role;
grant create procedure to bank_role;
grant create sequence to bank_role;
grant create table to bank_role;
grant create trigger to bank_role;
grant create type to bank_role;
grant create synonym to bank_role;
grant create view to bank_role;
grant create job to bank_role;
grant create materialized view to bank_role;
grant execute on sys.dbms_lock to bank_role;
grant execute on sys.owa_opt_lock to bank_role;

create user bank identified by oracle account unlock;
alter user bank quota unlimited on users;
grant bank_role to bank;

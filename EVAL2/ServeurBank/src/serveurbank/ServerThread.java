package serveurbank;

import MessageLib.Message;
import Server.Manager;
import SocketFacilities.MySSLSocket;
import SocketFacilities.MySocket;

import java.io.IOException;
import java.net.SocketException;
import server.ServerGUI;

public class ServerThread implements Runnable{

    private MySSLSocket clientSocket;
    private Manager requestManager;
    private ServerGUI serverGUI;

    public ServerThread(MySSLSocket clientSocket, Manager requestManager, ServerGUI serverGUI){
        this.clientSocket = clientSocket;
        this.requestManager = requestManager;
        this.serverGUI = serverGUI;
    }

    public void run()
    {
        Message message;
        while(true) 
        {
            
            try {
               message = (Message) clientSocket.ReceiveObj();
               
                serverGUI.printLog("[Thread] RequestReceived [" + message + "]");

                message = requestManager.Traitement(message);
                if(message != null)
                {
                    clientSocket.SendObj(message);
                    serverGUI.printLog("[Thread] ResponseSent [" + message + "]");
                }
                else
                {
                    clientSocket.SendObj(new Message(-1,"Internal Error"));
                   
                    serverGUI.printLog("[Thread] ResponseSent [internal error]");
                }
            }
            catch (SocketException e){
                serverGUI.printLog("[Thread] Connection Ended");
                return;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) 
            {
                e.printStackTrace();
                try
                {
                    clientSocket.SendObj(new Message(-1,"Internal Error"));
                    serverGUI.printLog("[Thread] ResponseSent [Internal error !]");
                }
                catch (IOException ex) 
                {
                    ex.printStackTrace();
                }
            }
             
        }
    }
}

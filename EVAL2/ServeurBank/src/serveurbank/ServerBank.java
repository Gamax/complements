/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serveurbank;

import DBFacilities.JDBCRequestBank;
import PARP.PARPServer;
import Server.Manager;
import SocketFacilities.ISocket;
import SocketFacilities.MySSLSocket;
import SocketFacilities.MySSLSocketServer;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.Server;
import server.ServerGUI;

/**
 *
 * @author Steph
 */
public class ServerBank extends server.Server {
    
    private JDBCRequestBank BDLinkBank ;
    
    @Override
    protected ISocket setSocket(int port) throws IOException
    {
        
        MySSLSocketServer socket =  new MySSLSocketServer(port,true);
        String keystore = "bank_keystore" ;
        int code_erreur = socket.init("Banking", "Banking",keystore);
        System.out.println(code_erreur);
        if(code_erreur<0)
            return null ;
        return socket ;
    }  
    
    @Override
    protected void setBDLink() throws SQLException, NullPointerException, ClassNotFoundException
    {
        BDLinkBank = new JDBCRequestBank();
    }
    
    @Override
    protected Manager setManager(ISocket socketService) throws NullPointerException, SQLException, ClassNotFoundException
    {
        return (Manager) new PARPServer(BDLinkBank);
    }
    
    @Override
    protected void RunServer(int nbThread, ISocket Socket, ServerGUI servergui)
   {
        MySSLSocketServer listeningSocket = (MySSLSocketServer) Socket ;
       
        ExecutorService threadPool = Executors.newFixedThreadPool(nbThread);
        servergui.printLog("Thread pool initialized");
        
        try {
            requestManager = setManager(null) ;
            
        } catch (NullPointerException | ClassNotFoundException | SQLException ex) {
            servergui.printLog("Cannot initialize core functionnalities, stopping in 5 secondes...");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex1) {
                Logger.getLogger(ServerBank.class.getName()).log(Level.SEVERE, null, ex1);
            }
            System.exit(1); 
        }
        while(true){
            MySSLSocket clientSocket;
            try {
                clientSocket = listeningSocket.Accept();
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
                return;
            }
            try {
                ServerThread srvThread = new ServerThread(clientSocket, requestManager , servergui);
                threadPool.execute(srvThread);
                servergui.printLog("Connection accepted");
            } catch (NullPointerException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }   
}

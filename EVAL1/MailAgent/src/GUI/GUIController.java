/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JFileChooser;
import java.io.File;
import maillib.Mail;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import java.awt.Image;
import java.io.File;
import ThreadMail.MailReader;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Header;
import javax.mail.MessagingException;

/**
 *
 * @author Steph
 */
public class GUIController {
    private Login log;
    private Mailbox mb;
    private NewMail nm;
    private ViewMail vm;
    private NewAttachment na;
    private ViewAttachment va ;
    private ViewTrace vt ;
    private String username ;
    private Mail mailcontrol ;
    
    public GUIController()
    {
        log = new Login();
        log.setController(this);
        mb = new Mailbox();
        mb.setController(this);
        nm = new NewMail();
        nm.setController(this);
        vm = new ViewMail();
        vm.setController(this);
        na = new NewAttachment();
        na.setController(this);
        va = new ViewAttachment();
        va.setController(this);
        vt = new ViewTrace();
        vt.setController(this);
        log.setVisible(true);
        msg = null;
    }
    
    
    
    // LOGIN
    public void authenticate(String login, String password)
    {
        String domain = login.substring(login.indexOf('@')+1);
        //TODO : add condition
        
        mailcontrol = new Mail(domain,login,password);
        MailReader mailreader = new MailReader(this);
        mailreader.start();
        
        
        log.setVisible(false);
        username = log.getUsername();
        JOptionPane.showMessageDialog(null, "Welcome " + username + " !","Welcome",JOptionPane.INFORMATION_MESSAGE );

        log.setVisible(false);
        mb.setTitle("MAILBOX : " + username);
        //MailgetListeMessage();
        mb.setVisible(true);
        log.dispose();
        
        
    }
    
    
    private Message[] msg ;
    
    // MAILBOX
    public synchronized void MailgetListeMessage()
    {
       DefaultTableModel mtm = mb.getTableModel() ;
       
       List<String> messagesIDRead = new ArrayList<> ();
       List<String> messagesIDToDelete = new ArrayList<> ();
        Vector vec = new Vector();
        int i ;
        

        i=0 ;
        while(i<mtm.getRowCount())
        {
            if(String.valueOf(mtm.getValueAt(i, 1)) == "true")
                messagesIDToDelete.add(String.valueOf(mtm.getValueAt(i, 0)));
           if(String.valueOf(mtm.getValueAt(i, 2)) == "true")
                messagesIDRead.add(String.valueOf(mtm.getValueAt(i, 0)));
            mtm.removeRow(i);
        }
        try 
        {
            msg = null;
            msg = mailcontrol.getMessages();
            for (i=msg.length-1;i>-1;i--)
            {
                
                vec.add(msg[i].getMessageNumber());
                if(messagesIDToDelete.contains(String.valueOf(msg[i].getMessageNumber())))
                    vec.add(true);
                else
                    vec.add(false);
                if(messagesIDRead.contains(String.valueOf(msg[i].getMessageNumber())))
                    vec.add(true);
                else
                    vec.add(false);
                vec.add(msg[i].getFrom()[0]);
                vec.add(msg[i].getSubject());
                vec.add(msg[i].getSentDate().toLocaleString());

                mtm.addRow(vec);
                vec = new Vector();
                mb.setListMessages(mtm);
            }
        }
        catch(ClassCastException e)
        {
            JOptionPane.showMessageDialog(null, "Error : unable to add new fields !","Error",JOptionPane.ERROR_MESSAGE);
        }

        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }

        
    }
    
    public void MailnewMessage()
    {
        nm.setFrom(username);
        na.setAttach(null);
        mb.setVisible(false);
        nm.setVisible(true);
        
    }
    
    Message msgEnCours ;
    public void MailreadMessage(String id)
    {
        try 
        {
            if(msg != null)
            {
                int i;
                for(i=0 ; i<msg.length ; i++)
                {
                    if(msg[i].getMessageNumber() == Integer.parseInt(id))
                        break ;
                }
                try{
                msgEnCours = msg[i] ;
                Multipart msgMP = (Multipart)msg[i].getContent();
                int np = msgMP.getCount();
                
                if(np > 1)
                    vm.setButtonAttachment(true);
                else
                    vm.setButtonAttachment(false);

                vm.setFrom(String.valueOf(msgEnCours.getFrom()[0]));
                vm.setObject(String.valueOf(msgEnCours.getSubject()));
                      
                Part part = msgMP.getBodyPart(0);
                if(part.isMimeType("text/plain"))
                    vm.setMessage(String.valueOf(part.getContent()));
                else
                    vm.setMessage("<Cannot retrive mail content !>");
                }
                catch(Exception e)
                {
                    vm.setButtonAttachment(false);
                    vm.setFrom(String.valueOf(msgEnCours.getFrom()[0]));
                    vm.setObject(String.valueOf(msgEnCours.getSubject()));
                    vm.setMessage(String.valueOf(msgEnCours.getContent()));
                }

                mb.setVisible(false);
                vm.setVisible(true);
            }
            else
                JOptionPane.showMessageDialog(null, "Error : can't read message, try again later !","Error",JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception e)
        {
             System.out.println("Errreur indéterminée : " + e.getMessage());
        }
    }
    
    public void MaildeleteMessage(ArrayList<String> idMailToDelete)
    {
        try {
            if(idMailToDelete.size()>0)
                mailcontrol.deleteMessages(idMailToDelete);
        } catch (MessagingException ex) {
            JOptionPane.showMessageDialog(null, "Error : can't delete messages ! " + ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void MailTrace(String id)
    {
        try {
            Enumeration<Header> headers = mailcontrol.getAMessage(id).getAllHeaders();
            Header header ; 
            String name ;
            String value ; 
            DefaultTableModel dtm = new DefaultTableModel();
            dtm.addColumn("Name of header");
            dtm.addColumn("Value of header");
            DefaultTableModel tm = new DefaultTableModel();
            tm.addColumn("RECEIVED");
            Vector vec , vecTrace;
            while(headers.hasMoreElements())
            {
                vec = new Vector();
                header = headers.nextElement() ;
                name = header.getName();
                value = header.getValue();
                vec.add(name);
                vec.add(value);
                dtm.addRow(vec);
                
                if(name.startsWith("Received"))
                {
                    vecTrace = new Vector();
                    vecTrace.add(value);
                    tm.addRow(vecTrace);
                }
            }
            
            vt.setHeaders(dtm);
            vt.setTrace(tm);
            vt.setVisible(true);
        } catch (MessagingException ex) 
        {
            JOptionPane.showMessageDialog(null, "Error : can't trace this message !","Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    //NEWMAIL
    public void NewSend(String from, String to, String object, String mail)
    {
        try
        {
           File attach = na.getAttach() ;
           if (attach == null)
                mailcontrol.sendMail(from, to, object, mail);
            else
                mailcontrol.sendMailAttach(from, to, object, mail, attach.getAbsolutePath());
            nm.setVisible(false);
            mb.setVisible(true);
        }
        catch(MessagingException e)
        {
            JOptionPane.showMessageDialog(null, "Error : can't send message : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error : can't send message : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void NewCancel()
    {
        nm.resetInterface();
        nm.setVisible(false);
        mb.setVisible(true);
        MailgetListeMessage();
    }
    
    public void NewAttachment()
    {
        na.setAttach(null);
        na.setVisible(true);
    }
    
    //VIEWMAIL
    public void ViewAnswer(String originalFrom)
    {
        nm.setFrom(username);
        nm.setTo(originalFrom);
        vm.resetInterface();
        vm.setVisible(false);
        nm.setVisible(true);
    }
    
    public void ViewReturn()
    {
        vm.resetInterface();
        vm.setVisible(false);
        mb.setVisible(true);
        MailgetListeMessage();
    }
    
    public void ViewAttachment()
    {
        try 
        {
            Multipart msgMP = (Multipart)msgEnCours.getContent();
            int np = msgMP.getCount();
            for (int j=0; j<np; j++)
            {
                Part p = msgMP.getBodyPart(j);
                String d = p.getDisposition();
                if (d!=null && d.equalsIgnoreCase(Part.ATTACHMENT))
                {
                    InputStream is = p.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    int c;
                    while ((c = is.read()) != -1) baos.write(c);
                    baos.flush();
                    String nf = "..\\Download\\" + p.getFileName();
                    FileOutputStream fos =new FileOutputStream(nf);
                    baos.writeTo(fos);
                    fos.close();
                    
                    System.out.println("Pièce attachée " + nf + " récupérée"); 
                    va.setImage(nf);
                    va.setVisible(true);
                }
            }
        }
        catch (Exception e) 
        {
            JOptionPane.showMessageDialog(null, "Error : can't retrieve attachment, exception : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }

    public File addPath()
    {
        JFileChooser pathDialog = new JFileChooser();
        pathDialog.setCurrentDirectory(new java.io.File("..\\Attachment\\"));
        if(pathDialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
            return pathDialog.getSelectedFile();
        else
            return null;
    }
    
    public void attachCancel()
    {
        na.setVisible(false);
    }
}

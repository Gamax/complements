/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maillib;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;

/**
 *
 * @author Steph
 */
public class MailLib {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Mail mailcontrol = new Mail("gmail.com","2327hepl.2018@gmail.com","L3m0nNation");
            
            mailcontrol.sendMailAttach("2327hepl.2018@gmail.com","2327hepl.2018@gmail.com","team liegoise","rpz inpres !","D:\\InpresHelmo.gif");
            Message[] msg = mailcontrol.getMessages();
            Multipart msgMP ;
           
           int i ;
           for(i=0;i<msg.length;i++)
           {
                if (msg[i].isMimeType("text/plain"))
                {
                    System.out.println("Message : " + + msg[i].getMessageNumber() + " : " + msg[i].getFrom()[0] + " : "+ msg[i].getSubject() + " : " + msg[i].getSentDate().toLocaleString() );
                    System.out.println("Content : " + String.valueOf(msg[i].getContent()));
                }
                else
                {
                    msgMP = (Multipart)msg[i].getContent();
                    int np = msgMP.getCount();
                    for (int j=0; j<np; j++)
                    {
                        System.out.println("--Composante n° " + j);
                        Part p = msgMP.getBodyPart(j);
                        String d = p.getDisposition();
                        if (p.isMimeType("text/plain"))
                            System.out.println("Texte : " + (String)p.getContent());
                        if (d!=null && d.equalsIgnoreCase(Part.ATTACHMENT))
                        {
                            InputStream is = p.getInputStream();
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            int c;
                            while ((c = is.read()) != -1) baos.write(c);
                            baos.flush();
                            String nf = p.getFileName();
                            FileOutputStream fos =new FileOutputStream(nf);
                            baos.writeTo(fos);
                            fos.close();
                            System.out.println("Pièce attachée " + nf + " récupérée");  
                        }
                        System.out.println("Message : " + + msg[i].getMessageNumber() + " : " + msg[i].getFrom()[0] + " : "+ msg[i].getSubject() + " : " + msg[i].getSentDate().toLocaleString() );
                        System.out.println("Content : " + ((Multipart)msg[i].getContent()).getBodyPart(0).getContent());
                    }
                    
                }
            }
        } 
        catch (Exception e) 
        {
            System.out.println("Error : " + e.getMessage());
        }
        
        
        
    }
    
}

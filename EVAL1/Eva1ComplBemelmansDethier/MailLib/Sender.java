/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MailLib;

/**
 *
 * @author Steph
 */


import java.util.List;
import java.util.Properties;
import javax.mail. * ;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

public class Sender {
    private Session session ; 
    private Properties properties ;
    private String host ;
    private String domain ;
    private static String charset = "iso-8859-1"; 
    
    public Sender()
    {
        
        
        host = "u2.tech.hepl.local";
        properties = System.getProperties();
        properties.put("mail.smtp.host", host); 
        properties.put("file.encoding", charset); 
        session = Session.getDefaultInstance(properties, null);
    }
    
    public Sender(String Domain, String mail)
    {
        host = mail;
        domain = Domain;
        
        properties = System.getProperties();
        properties.put("mail.smtp.host", host); 
        properties.put("file.encoding", charset); 
        
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.from", host);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true"); 
        properties.put("mail.debug", "true");
        properties.put("file.encoding", "iso-8859-1");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        session = Session.getDefaultInstance(properties, null);
    }
    
    
    public boolean sendMailNoAttach(String from, String to, String subject, String content) 
    {
        try {
            if(from.contains("@"))
            {
                from = from.substring(0,from.indexOf(from));
                 JOptionPane.showMessageDialog(null, "Format " + from, "Error : login",JOptionPane.ERROR_MESSAGE );   
            }
            MimeMessage msg = new MimeMessage (session);
            msg.setFrom (new InternetAddress (from));
            msg.setRecipient (Message.RecipientType.TO, new InternetAddress (to));
            msg.setSubject(subject);
            msg.setText (content);
            Transport.send(msg);
            
            return true ;
        } catch (MessagingException e) {
            return false;
        }
        
    }
}

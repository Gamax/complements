/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maillib;

/**
 *
 * @author Steph
 */

import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import javax.activation.*; 
import javax.mail.* ;
import javax.mail.internet.*;


public class Mail {
    
    private Session session ; 
    private Properties properties ;
    private String host ;
    private String domain ;
    private static String charset = "iso-8859-1"; 
    private String password ;
    private MimeMessage message ;
    
    public Mail()
    {
        host = "bemelmans";
        domain = "u2.tech.hepl.local";
        password = "toto" ;
        properties = System.getProperties();
        properties.put("mail.smtp.host", host); 
        properties.put("file.encoding", charset); 
        session = Session.getDefaultInstance(properties, null);
    }
    
    public Mail(String Domain, String user, String pswd)
    {
        host = user;
        domain = Domain;
        password = pswd ;
        properties = System.getProperties();
        
        if(domain.contains("gmail"))
            connectGmail();
        else
        {
            if(domain.contains("hotmail"))
                connectHotmail();
            else
            {
                if(domain.contains("u2"))
                    connectU2();
                else
                {
                    if(domain.contains("insx"))
                        connectInxs();
                }
            }
        }
        
        //session = Session.getDefaultInstance(properties,null);
        
    }
    
    
    
    private void connectHotmail()
    {
        String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        properties.setProperty("mail.pop3s.port", "995");
        properties.setProperty("mail.pop3.ssl.enable", "true");
        properties.setProperty("mail.pop3s.socketFactory.class", SSL_FACTORY);
        properties.setProperty("mail.pop3s.socketFactory.fallback", "false");
        properties.setProperty("mail.pop3s.port", "995");
        properties.setProperty("mail.pop3s.socketFactory.port", "995");

        session = Session.getDefaultInstance(properties, new Authenticator() {
        protected PasswordAuthentication  getPasswordAuthentication() {
        return new PasswordAuthentication(host, password);}
        });
    }
    
    private void connectGmail()
    {
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.from", host);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true"); 
        properties.put("file.encoding", "iso-8859-1");
        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        //properties.put("mail.debug", "true");
        properties.put("mail.pop3.host", "pop.gmail.com");
        properties.put("mail.pop3.auth","true");
        properties.put("mail.pop3.port", "995");
        properties.put("mail.pop3.starttls.enable","true");
        properties.put("mail.disable.top", "true");
        properties.put("mail.pop3.disablecapa", "true");
        properties.put("mail.pop3.socketFactory" , "995" );
        properties.put("mail.pop3.socketFactory.class" , "javax.net.ssl.SSLSocketFactory" );
        
        session = Session.getDefaultInstance(properties, new Authenticator() {
        protected PasswordAuthentication  getPasswordAuthentication() {
        return new PasswordAuthentication(host, password);}
        });
        
    }
    
    private void connectU2()
    {
        properties.put("mail.pop3.host", "u2.tech.hepl");
        properties.put("mail.disable.top", true);
        session = Session.getDefaultInstance(properties, null); 

    }
    
    private void connectInxs()
    {
        properties.put("mail.pop3.host", "inxs.aileinfo");
        properties.put("mail.disable.top", true);
        session = Session.getDefaultInstance(properties, null); 
    }
    
    public void sendMail(String from, String to, String subject, String content) throws AddressException, MessagingException 
    {
        MimeMessage msg = new MimeMessage (session);
        msg.setFrom (new InternetAddress (from));
        msg.setRecipient (Message.RecipientType.TO, new InternetAddress (to));
        msg.setSubject(subject);
        msg.setText (content); 

        Transport.send(msg);
    }
    
    public void sendMailAttach(String from, String to, String subject, String content, String PathAttach) throws AddressException, MessagingException 
    {
        MimeMessage msg = new MimeMessage (session);
        msg.setFrom (new InternetAddress (from));
        msg.addRecipient (Message.RecipientType.TO, new InternetAddress (to));
        msg.setSubject(subject);

        Multipart multipart = new MimeMultipart();
        
        //Texte :
        MimeBodyPart msgBP = new MimeBodyPart();
       // msgBP.setDisposition(MimeBodyPart.INLINE);
        msgBP.setText(content);
        
        multipart.addBodyPart(msgBP); 

        //Piece jointe :
        if(PathAttach!=null)
        {
            msgBP = new MimeBodyPart();
            
            DataSource so = new FileDataSource (PathAttach);
            msgBP.setDataHandler (new DataHandler (so));
            
            msgBP.setFileName(PathAttach.substring(PathAttach.lastIndexOf("\\")+1));
            msgBP.setDisposition(Part.ATTACHMENT);
            multipart.addBodyPart(msgBP); 
        }
        
        msg.setContent(multipart);
        System.out.println(PathAttach );
        Transport.send(msg);    
    }
    
    public Message[] getMessages() throws NoSuchProviderException, MessagingException
    {
        Message[] msg ;
        //Réception des messages
        Store store = session.getStore("pop3");
        store.connect("pop."+domain, host, password);

        Folder fold = store.getFolder("INBOX");
        fold.open(Folder.READ_ONLY);

        msg = fold.getMessages();
        
        return msg ; 
    }
    
    public Message getAMessage(String id) throws MessagingException
    {
        Message[] msg = getMessages() ;
        for(int i = 0 ; i < msg.length; i++)
        {
            if(msg[i].getMessageNumber() == Integer.parseInt(id))
                return msg[i];
        }
        
        return null ;
    }
    
    public void trace(String id) throws MessagingException
    {
        Enumeration<Header> headers = getAMessage(id).getAllHeaders();
        Header header ; 
        while(headers.hasMoreElements())
        {
            header = headers.nextElement() ;
            System.out.println("Headers : " + header.getName() + " " + header.getValue());
        }
    }
    
    public void deleteMessages(List id) throws NoSuchProviderException, MessagingException
    {
        Message[] msg ;
        Store store = session.getStore("pop3");
        store.connect("pop."+domain, host, password);

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_ONLY);

        msg = folder.getMessages();
        Flags deleted = new Flags("DELETED");  
        for(int i = 0 ; i < id.size() ; i++)
        {
            if(id.contains(String.valueOf(msg[i].getMessageNumber())))
            {
                System.out.println("To delete : " + msg[i].getMessageNumber() + " : " + msg[i].getFrom() + " : " + msg[i].getSubject());
                //TODO : delete
                msg[i].setFlag(Flags.Flag.DELETED, true);
            }
        }
        if(folder.isOpen())
            folder.close(true);
    }
}

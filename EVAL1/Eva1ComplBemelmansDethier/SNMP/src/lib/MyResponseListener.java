/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.smi.VariableBinding;

/**
 *
 * @author Thomas
 */
public class MyResponseListener implements ResponseListener {
    private Snmp snmpManager;
    public boolean isNode;
    public VariableBinding vbresponse;
    
    public MyResponseListener(Snmp snmpManager)
    {
        this.snmpManager = snmpManager;
    }
    
    public void onResponse(ResponseEvent event)
    {
        ((Snmp)event.getSource()).cancel(event.getRequest(), this);
        
        PDU response = event.getResponse();
        
        if(response.getErrorStatusText().equals("Success"))
        {
             vbresponse = response.get(0);
             isNode = false;
        }
        else
        {
        isNode = true;
        }
        
        synchronized(snmpManager){
            snmpManager.notify();
        }
    }
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ThreadMail;

import GUI.GUIController;
import java.util.concurrent.TimeUnit;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;
import javax.swing.DefaultListModel;

/**
 *
 * @author Steph
 */
public class MailReader extends Thread {
    private GUIController master;
    public MailReader(GUIController Master) 
    {
        master = Master ;
    }
    
    public void run()
    {
        while(true)
        {
            
            try 
            {
                master.MailgetListeMessage();   
                TimeUnit.MINUTES.sleep(5);
            }
            catch (InterruptedException ex) 
            {
                System.out.println("Erreur: " + ex);
            }
        }
    }
}

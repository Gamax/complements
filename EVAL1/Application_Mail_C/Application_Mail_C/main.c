#include <stdio.h>
#include <stdlib.h>

//Traitement menu
void readMail();
void sendMail();
char * choixDomain();
char * getUsername();
char * getPassword();
int connexionPOP(char *domain, char *username, char *password);
int connexionSMTP(char *domain, char *username, char *password);
void createMail(char *domain, char *username, char *password);
void listMail();

char tab1[] = "u2.tech.hepl.local";
char tab2[] = "inxs.aileinfo";
char tab3[] = "gmail.com";
char password[30];
char username[30];

int main()
{
    int choix ;
    do
    {
        printf("\nMENU :\n");
        printf("------\n\n");
        printf("1. Read mail\n");
        printf("2. Send mail\n");
        printf("3. Quitter\n");
        printf("Select an option : ");
        scanf("%d",&choix);
        fflush(stdout);

        switch(choix)
        {
            case 1 :    readMail() ;
                        break ;
            case 2 :    sendMail();
                        break ;
            case 3 :    break ;
            default :   printf("Choix incorrect !");
                        fflush(stdin);
                        break;
        }
    }while(choix != 3);

    return 0;
}


//Traitement
void readMail()
{
    char *domain = choixDomain();
    char *username = getUsername();
    char *password = getPassword();

    if(connexionPOP(domain,username,password)==0)
        listMails();
    else
        printf("Cannot connect to server !");
}

void sendMail()
{
    char *domain = choixDomain();
    char *username = getUsername();
    char *password = getPassword();
    if(connexionSMTP(domain,username,password)==0)
        createMail();
    else
        printf("Cannot connect to server !");

}


int connexionPOP(char *domain, char *username, char *password)
{
    printf("\nTrying to connect with %s@%s...",username,domain,password);
    fflush(stdin);
}

int connexionSMTP(char *domain, char *username, char *password)
{
    printf("\nTrying to connect with %s@%s...",username,domain,password);
    fflush(stdin);
}







//UTils
char * getUsername()
{
    printf("Entrez votre nom d'utilisateur : ");
    scanf("%s",username);
    fflush(stdout);

    return username ;
}

char * getPassword()
{
    printf("Entrez votre mot de passe : ");
    scanf("%s",password);
    fflush(stdout);

    return password ;
}

char * choixDomain()
{
    int choix ;
    char * pTab = NULL ;

    printf("Choose a domain to connect to :\n");
    printf("-------------------------------\n\n");
    printf("1. u2.tech.hepl.local\n");
    printf("2. inxs.aileinfo\n");
    printf("3. gmail.com\n");
    scanf("%d",&choix);
    fflush(stdout);
    switch(choix)
    {
        case 1 :    pTab = tab1;
                    break;
        case 2 :    pTab = tab2;
                    break;
        case 3 :    pTab = tab3;
                    break;
        default :   printf("Invalid choice ! ");
                    pTab = NULL;
                    break;
    }

    return pTab;
}

package application_mail;

import java.awt.Color;
import java.awt.Desktop;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.swing.ImageIcon;


public class Lecture_Message extends javax.swing.JDialog 
{
    private Message msg;
    
    public Lecture_Message(java.awt.Frame parent, boolean modal) 
    {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);
    }
    
    public Lecture_Message(java.awt.Frame parent, boolean modal, Message m) 
    {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);
        jTextAreaMessage.setEditable(false);
        jTextAreaTracer.setEditable(false);
        
        msg = m;
        
        try
        {
            jLabelFrom.setText(m.getFrom()[0].toString());
            jLabelObjet.setText(m.getSubject());
            if(m.isMimeType("text/plain"))
                jTextAreaMessage.setText((String)m.getContent());
            else
            {
                if(m.isMimeType("multipart/mixed"))
                {
                    Multipart msgMP = (Multipart)m.getContent();
                    int n = msgMP.getCount();
                
                    for(int i = 0; i < n; i++)
                    {
                        Part p = msgMP.getBodyPart(i);
                        String d = p.getDisposition();
                        
                        //jTextAreaMessage.setText("+++++++" + p.getContentType() + "+++++++++++++++++\n");
                        
                        if(p.isMimeType("text/plain"))
                            jTextAreaMessage.setText((String)p.getContent());
                        
                        if(d != null && d.equalsIgnoreCase(Part.ATTACHMENT))
                        {
                            if(p.isMimeType("image/gif") || p.isMimeType("image/jpeg") || p.isMimeType("image/png"))
                            {
                                InputStream is = p.getInputStream();
                                ByteArrayOutputStream ba = new ByteArrayOutputStream();
                                int c;
                                while((c = is.read()) != -1)
                                    ba.write(c);
                                ba.flush();
                                
                                ImageIcon icon = new ImageIcon(ba.toByteArray());
                                Image image = new Image(parent, false, icon, p.getFileName());
                                image.setVisible(true);
                            }
                            
                            if(p.isMimeType("application/octet-stream"))
                            {
                                Desktop desk = Desktop.getDesktop();
                                desk.open(new File(p.getFileName()));
                            }
                            
                            jLabelFichier.setText(jLabelFichier.getText() + "  " + p.getFileName());
                        }
                    }
                }
            }
        }
        catch (IOException ex) 
        {
            System.out.println("Erreur: " + ex);
        } 
        catch (MessagingException ex) 
        {
            System.out.println("Erreur: " + ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonFermer = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabelFrom = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabelObjet = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaMessage = new javax.swing.JTextArea();
        jLabelFichier = new javax.swing.JLabel();
        jButtontracer = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaTracer = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Lecture message");

        jButtonFermer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonFermer.setText("Fermer");
        jButtonFermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFermerActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("From :");

        jLabelFrom.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Objet :");

        jLabelObjet.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Message :");

        jTextAreaMessage.setColumns(20);
        jTextAreaMessage.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextAreaMessage.setRows(5);
        jScrollPane1.setViewportView(jTextAreaMessage);

        jLabelFichier.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jButtontracer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtontracer.setText("Tracer");
        jButtontracer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtontracerActionPerformed(evt);
            }
        });

        jTextAreaTracer.setColumns(20);
        jTextAreaTracer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextAreaTracer.setRows(5);
        jScrollPane2.setViewportView(jTextAreaTracer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFrom))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelObjet))
                            .addComponent(jLabel3)
                            .addComponent(jLabelFichier))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtontracer)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonFermer)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabelFrom))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabelObjet))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelFichier)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtontracer)
                    .addComponent(jButtonFermer))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonFermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFermerActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonFermerActionPerformed

    private void jButtontracerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtontracerActionPerformed
        try 
        {
            Enumeration e = msg.getAllHeaders();
            Header h = (Header)e.nextElement();
            while(e.hasMoreElements())
            {
                jTextAreaTracer.setText(jTextAreaTracer.getText() + "\n" + h.getName() + "-->" + h.getValue());
                h = (Header)e.nextElement();
            }
        } 
        catch (MessagingException ex) 
        {
            System.out.println("Erreur: " + ex);
        }
    }//GEN-LAST:event_jButtontracerActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Lecture_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Lecture_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Lecture_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Lecture_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Lecture_Message dialog = new Lecture_Message(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonFermer;
    private javax.swing.JButton jButtontracer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabelFichier;
    private javax.swing.JLabel jLabelFrom;
    private javax.swing.JLabel jLabelObjet;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextAreaMessage;
    private javax.swing.JTextArea jTextAreaTracer;
    // End of variables declaration//GEN-END:variables
}

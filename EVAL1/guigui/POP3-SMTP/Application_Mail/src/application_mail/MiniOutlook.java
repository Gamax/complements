package application_mail;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.swing.DefaultListModel;

/**
 *
 * @author Guillaume
 */
public class MiniOutlook extends javax.swing.JFrame 
{
    private Properties _propFichier = new Properties();
    private Properties _propMail = new Properties();
    private Session session;
    private ThreadMail tm;
    private Message[] msg;
    
    public MiniOutlook() 
    {
        initComponents();
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);
        
        LoadPropertiesFichier();
       
        _propMail = System.getProperties();
        
        //Propriété pour l'envoie
        _propMail.put("mail.smtp.host", _propFichier.getProperty("SERVERENVOIE"));
        _propMail.put("mail.smtp.port", _propFichier.getProperty("PORTENVOIE"));
        _propMail.put("mail.from", _propFichier.getProperty("MAIL"));
        _propMail.put("mail.smtp.starttls.enable", "true");
        _propMail.put("mail.smtp.auth", "true"); 
        _propMail.put("mail.debug", "true");
        _propMail.put("file.encoding", "iso-8859-1");
        _propMail.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        
        
        //Propriété pour la réception
        _propMail.put("mail.pop3.host", _propFichier.getProperty("SERVERRECEP"));
        _propMail.put("mail.pop3.auth","true");
        _propMail.put("mail.pop3.port", _propFichier.getProperty("PORTRECEP"));
        _propMail.put("mail.pop3.starttls.enable","true");
        _propMail.put("mail.disable.top", "true");
        _propMail.put("mail.pop3.disablecapa", "true");
        _propMail.put("mail.pop3.socketFactory" , _propFichier.getProperty("PORTRECEP") );
        _propMail.put("mail.pop3.socketFactory.class" , "javax.net.ssl.SSLSocketFactory" );
        
        session = Session.getDefaultInstance(_propMail, new Authenticator() {
        protected PasswordAuthentication  getPasswordAuthentication() {
        return new PasswordAuthentication(_propFichier.getProperty("MAIL"), _propFichier.getProperty("PWD"));}
        });
        
        tm = new ThreadMail(session, jListMail, _propFichier);
        tm.start();
    }
    
    private void LoadPropertiesFichier()
    {
        InputStream input;
        try 
        {
            input = new FileInputStream(System.getProperty("user.dir") + System.getProperty("file.separator") + "data" + System.getProperty("file.separator") + "config.properties");

            _propFichier.load(input);
	} 
        catch (IOException ex)
        {
            System.out.println("Erreur fichier properties " + ex);
        }
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jListMail = new javax.swing.JList<>();
        jButtonNew = new javax.swing.JButton();
        jButtonReload = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mini Outlook");

        jListMail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jListMail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListMailMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jListMail);

        jButtonNew.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonNew.setText("Nouveau message");
        jButtonNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewActionPerformed(evt);
            }
        });

        jButtonReload.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jButtonReload.setText("Reload");
        jButtonReload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReloadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1045, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonNew)
                            .addComponent(jButtonReload))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonNew)
                .addGap(18, 18, 18)
                .addComponent(jButtonReload)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNewActionPerformed
        New_Message nm = new New_Message(this, true, _propFichier.getProperty("MAIL"), session);
        nm.setVisible(true);
    }//GEN-LAST:event_jButtonNewActionPerformed

    private void jListMailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListMailMouseClicked
        Lecture_Message lm = new Lecture_Message(this, false, tm.getMessage()[tm.getMessage().length - 1 - jListMail.getSelectedIndex()]);
        lm.setVisible(true);
    }//GEN-LAST:event_jListMailMouseClicked

    private void jButtonReloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReloadActionPerformed
        DefaultListModel dlm = new DefaultListModel();
            
        try 
        {
            //Réception des messages
            Store str = session.getStore("pop3");
            str.connect(_propFichier.getProperty("SERVERRECEP"), _propFichier.getProperty("MAIL"), _propFichier.getProperty("PWD"));

            Folder fold = str.getFolder("INBOX");
            fold.open(Folder.READ_ONLY);

            msg = fold.getMessages();
            tm.setMessage(msg);

            for(int i = msg.length-1; i >= 0; i--)
            {
                dlm.addElement(msg[i].getFrom()[0] + " " + msg[i].getSubject() + "       " + msg[i].getSentDate().toLocaleString());
                jListMail.setModel(dlm);
            }
        } 
        catch (NoSuchProviderException ex) 
        {
            System.out.println("Erreur: " + ex);
        } 
        catch (MessagingException ex) 
        {
            System.out.println("Erreur: " + ex);
        }
    }//GEN-LAST:event_jButtonReloadActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MiniOutlook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MiniOutlook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MiniOutlook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MiniOutlook.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MiniOutlook().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonNew;
    private javax.swing.JButton jButtonReload;
    private javax.swing.JList<String> jListMail;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

package application_mail;

import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.swing.DefaultListModel;


public class ThreadMail extends Thread
{
    private javax.swing.JList<String> jListMail;
    private Properties _propFichier;
    private Session session;
    private Message[] msg;
    
    public ThreadMail(Session s, javax.swing.JList<String> list, Properties p)
    {
        jListMail = list;
        _propFichier = p;
        session = s;
    }
    
    public Message[] getMessage()
    {
        return msg;
    }
    
    public void setMessage(Message[] m)
    {
        msg = m;
    }
    
    public void run()
    {
        while(true)
        {
            DefaultListModel dlm = new DefaultListModel();
            
            try 
            {
                //Réception des messages
                Store str = session.getStore("pop3");
                str.connect(_propFichier.getProperty("SERVERRECEP"), _propFichier.getProperty("MAIL"), _propFichier.getProperty("PWD"));

                Folder fold = str.getFolder("INBOX");
                fold.open(Folder.READ_ONLY);

                msg = fold.getMessages();

                for(int i = msg.length-1; i >= 0; i--)
                {
                    dlm.addElement(msg[i].getFrom()[0] + " " + msg[i].getSubject() + "       " + msg[i].getSentDate().toLocaleString());
                    jListMail.setModel(dlm);
                }
                
                TimeUnit.MINUTES.sleep(5);
            } 
            catch (NoSuchProviderException ex) 
            {
                System.out.println("Erreur: " + ex);
            } 
            catch (MessagingException ex) 
            {
                System.out.println("Erreur: " + ex);
            }
            catch (InterruptedException ex) 
            {
                System.out.println("Erreur: " + ex);
            }
        }
    }
}

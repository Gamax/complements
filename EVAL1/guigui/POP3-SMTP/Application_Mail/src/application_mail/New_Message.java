package application_mail;

import java.awt.Color;
import java.io.File;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;


public class New_Message extends javax.swing.JDialog 
{
    private String mail;
    private Session session;
    private File[] file = new File[5];
    private int i = 0;
    private javax.swing.JFileChooser jFileChooser;
    
    public New_Message(java.awt.Frame parent, boolean modal) 
    {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);
    }
    
    public New_Message(java.awt.Frame parent, boolean modal, String m, Session s) 
    {
        super(parent, modal);
        initComponents();
        jFileChooser = new javax.swing.JFileChooser();
        jFileChooser.setCurrentDirectory(new java.io.File("C:\\Users\\Guillaume\\Downloads"));
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);
        
        mail = m;
        session = s;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaMessage = new javax.swing.JTextArea();
        jButtonEnvoyer = new javax.swing.JButton();
        jLabelDestinataire = new javax.swing.JLabel();
        jLabelObjet = new javax.swing.JLabel();
        jTextFieldDestinataire = new javax.swing.JTextField();
        jTextFieldObjet = new javax.swing.JTextField();
        jLabelMessage = new javax.swing.JLabel();
        jButtonFichier = new javax.swing.JButton();
        jLabelFichier = new javax.swing.JLabel();
        jLabelDigest = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Noveau message");

        jTextAreaMessage.setColumns(20);
        jTextAreaMessage.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jTextAreaMessage.setRows(5);
        jScrollPane1.setViewportView(jTextAreaMessage);

        jButtonEnvoyer.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonEnvoyer.setText("Envoyer");
        jButtonEnvoyer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnvoyerActionPerformed(evt);
            }
        });

        jLabelDestinataire.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabelDestinataire.setText("Destinataire :");

        jLabelObjet.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabelObjet.setText("Objet :");

        jTextFieldDestinataire.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jTextFieldObjet.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabelMessage.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabelMessage.setText("Message :");

        jButtonFichier.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonFichier.setText("Joindre des fichiers");
        jButtonFichier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFichierActionPerformed(evt);
            }
        });

        jLabelFichier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabelDigest.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 954, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelObjet)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldObjet))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelDestinataire)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldDestinataire))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonEnvoyer))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMessage)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonFichier)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelFichier))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(148, 148, 148)
                                .addComponent(jLabelDigest)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelDestinataire)
                    .addComponent(jTextFieldDestinataire, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelObjet)
                    .addComponent(jTextFieldObjet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelMessage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonFichier)
                    .addComponent(jLabelFichier))
                .addGap(18, 18, 18)
                .addComponent(jLabelDigest)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonEnvoyer)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonEnvoyerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEnvoyerActionPerformed
        if(!jTextFieldDestinataire.getText().equals("") && !jTextFieldObjet.getText().equals(""))
        {
            try 
            {
                MimeMessage msg = new MimeMessage(session);

                msg.setFrom(new InternetAddress(mail));
                msg.setRecipient(Message.RecipientType.TO, new InternetAddress(jTextFieldDestinataire.getText()));
                msg.setSubject(jTextFieldObjet.getText());
                
                if(jLabelFichier.getText().equals("") && jLabelDigest.getText().equals(""))
                    msg.setText(jTextAreaMessage.getText());
                else
                {
                    Multipart msgMP = new MimeMultipart();
                    
                    MimeBodyPart msgBP = new MimeBodyPart();
                    msgBP.setText(jTextAreaMessage.getText());
                    msgMP.addBodyPart(msgBP);
                    
                    if(!jLabelFichier.getText().equals(""))
                    {
                        for(int j = 0; j < i; j++)
                        {
                            DataSource d = new FileDataSource(file[j].getAbsolutePath());
                            msgBP = new MimeBodyPart();
                            msgBP.setDataHandler(new DataHandler(d));
                            msgBP.setFileName(file[j].getAbsolutePath());
                            msgMP.addBodyPart(msgBP);
                        }
                    }

                    msg.setContent(msgMP);
                }
                
                Transport.send(msg);

                dispose();
            } 
            catch (AddressException ex) 
            {
                System.out.println("Erreur: " + ex);
            } 
            catch (MessagingException ex) 
            {
                System.out.println("Erreur: " + ex);
            }
        }
        else
            showMessageDialog(this, "Veillez remplir tous les champs d'envoi !", "Error", ERROR_MESSAGE);
    }//GEN-LAST:event_jButtonEnvoyerActionPerformed

    private void jButtonFichierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFichierActionPerformed
        int returnVal = jFileChooser.showOpenDialog(this);

        if (returnVal == jFileChooser.APPROVE_OPTION) 
        {
            file[i] = jFileChooser.getSelectedFile();
            jLabelFichier.setText(jLabelFichier.getText() + "  " + file[i].getName());
            i++;
        }
    }//GEN-LAST:event_jButtonFichierActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(New_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(New_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(New_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(New_Message.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                New_Message dialog = new New_Message(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEnvoyer;
    private javax.swing.JButton jButtonFichier;
    private javax.swing.JLabel jLabelDestinataire;
    private javax.swing.JLabel jLabelDigest;
    private javax.swing.JLabel jLabelFichier;
    private javax.swing.JLabel jLabelMessage;
    private javax.swing.JLabel jLabelObjet;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaMessage;
    private javax.swing.JTextField jTextFieldDestinataire;
    private javax.swing.JTextField jTextFieldObjet;
    // End of variables declaration//GEN-END:variables
}

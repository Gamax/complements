package serveur_control;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.*;
import javax.swing.table.DefaultTableModel;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 *
 * @author Guillaume
 */
public class FrameSNMP extends javax.swing.JFrame 
{
    private TransportMapping transport;
    private SnmpListener listener;
    private Snmp snmp;
    private CommunityTarget target;
    private DefaultTableModel dtm;
    
    public FrameSNMP() 
    {
        initComponents();
        
        setLocationRelativeTo(null);
        getContentPane().setBackground(Color.LIGHT_GRAY);
        Table();
        
        try 
        {
            transport = new DefaultUdpTransportMapping();
            transport.listen();
            
            snmp = new Snmp(transport);
            target = new CommunityTarget();
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version1);
        } 
        catch (IOException ex) 
        {
            showMessageDialog(this, "Erreur : " + ex, "Error", ERROR_MESSAGE);
        }
    }
    
    private void Table()
    {
        jTableSNMP.setModel(new DefaultTableModel(new Object[][] {}, new String[] {"Name", "Value", "Type", "IP"}));
        dtm = (DefaultTableModel)jTableSNMP.getModel();
    }
    
    private boolean Trouve100(String s)
    {
        StringTokenizer st = new StringTokenizer(s, " ");
        
        while(st.hasMoreTokens())
        {
            String test = st.nextToken();
            int p = test.indexOf("%");
            int p100 = test.indexOf("100");
            if(p != -1 && p100 != -1)
                return true;
        }
        return false;
    }
    
    private boolean Ping()
    {
        boolean trouve = false;
        try 
        {
            //Test de la connexion
            Process p;
            String command = "ping -n 4 -w 1000 " + jTextFieldAddress.getText();
            BufferedReader buffer;

            p = Runtime.getRuntime().exec(command);
            if(p == null)
                System.out.println("Erreur de ping");

            buffer = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while((line = buffer.readLine()) != null) 
            {
                trouve = Trouve100(line);
                if(trouve == true)
                {
                    showMessageDialog(this, "L'adresse " + jTextFieldAddress.getText() + " ne repond pas.", "Error", ERROR_MESSAGE);
                    return trouve;
                }
            }
            buffer.close();
        } 
        catch (IOException ex) 
        {
            showMessageDialog(this, "Erreur Ping : " + ex, "Error", ERROR_MESSAGE);
        }
        return trouve;
    }
    
    private int[] OIDint()
    {
        String[] s = jComboBoxOID.getSelectedItem().toString().substring(1).split("\\.");
        int[] n = new int[s.length];
        for(int i = 0; i < s.length; i++)
            n[i] = Integer.parseInt(s[i]);
        
        return n;
    }
    
    private void Asynchronous(PDU pdu)
    {
        try 
        {
            listener = new SnmpListener(snmp, dtm);
            snmp.send(pdu, target, null, listener);
            synchronized(snmp)
            {
                snmp.wait();
            }
        } 
        catch (IOException | InterruptedException ex) 
        {
            showMessageDialog(this, "Erreur : " + ex, "Error", ERROR_MESSAGE);
        } 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelAddress = new javax.swing.JLabel();
        jTextFieldAddress = new javax.swing.JTextField();
        jLabelPort = new javax.swing.JLabel();
        jTextFieldPort = new javax.swing.JTextField();
        jLabelCommunity = new javax.swing.JLabel();
        jTextFieldCommunity = new javax.swing.JTextField();
        jLabelOID = new javax.swing.JLabel();
        jComboBoxOID = new javax.swing.JComboBox<>();
        jLabelOperations = new javax.swing.JLabel();
        jComboBoxOperations = new javax.swing.JComboBox<>();
        jButtonGO = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableSNMP = new javax.swing.JTable();
        jButtonConnexion = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SNMP");

        jLabelAddress.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabelAddress.setText("Address :");

        jTextFieldAddress.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextFieldAddress.setText("127.0.0.1");

        jLabelPort.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabelPort.setText("Port :");

        jTextFieldPort.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextFieldPort.setText("161");
        jTextFieldPort.setEnabled(false);

        jLabelCommunity.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabelCommunity.setText("Community :");

        jTextFieldCommunity.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextFieldCommunity.setText("2326PG");

        jLabelOID.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabelOID.setText("OID :");

        jComboBoxOID.setEditable(true);
        jComboBoxOID.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBoxOID.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", ".1.3.6.1.2.1.1.4.0", ".1.3.6.1.2.1.1.5.0", ".1.3.6.1.2.1.1.6.0", ".1.3.6.1.2.1.4.20", ".1.3.6.1.2.1.2.2.1.2" }));
        jComboBoxOID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxOIDActionPerformed(evt);
            }
        });

        jLabelOperations.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabelOperations.setText("Operations :");

        jComboBoxOperations.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBoxOperations.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "GETNEXT", "GET", "SET" }));

        jButtonGO.setBackground(new java.awt.Color(0, 153, 51));
        jButtonGO.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonGO.setText("GO");
        jButtonGO.setBorderPainted(false);
        jButtonGO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGOActionPerformed(evt);
            }
        });

        jTableSNMP.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTableSNMP);

        jButtonConnexion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButtonConnexion.setText("Tester la connexion");
        jButtonConnexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConnexionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelAddress)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelPort)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelCommunity)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldCommunity, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelOID)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxOID, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelOperations)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxOperations, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonGO))
                            .addComponent(jButtonConnexion))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAddress)
                    .addComponent(jTextFieldAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPort)
                    .addComponent(jTextFieldPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCommunity)
                    .addComponent(jTextFieldCommunity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelOID)
                    .addComponent(jComboBoxOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelOperations)
                    .addComponent(jComboBoxOperations, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonGO))
                .addGap(18, 18, 18)
                .addComponent(jButtonConnexion)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGOActionPerformed
        if(!jTextFieldAddress.getText().equals("") && !jTextFieldCommunity.getText().equals("") && !jComboBoxOID.getSelectedItem().toString().equals(""))
        {
            //Mise en place de SNMP
            target.setCommunity(new OctetString(jTextFieldCommunity.getText()));
            Address targetAddress = GenericAddress.parse("udp:" + jTextFieldAddress.getText() + "/" + jTextFieldPort.getText());
            target.setAddress(targetAddress);

            PDU pdu = new PDU();

            //GETNEXT
            if(jComboBoxOperations.getSelectedItem().toString().equals("GETNEXT"))
            {
                    int[] n = OIDint();

                    pdu.add(new VariableBinding(new OID(n)));
                    pdu.setType(PDU.GETNEXT);

                    Asynchronous(pdu);

                    jComboBoxOID.addItem(listener.getOID());
                    jComboBoxOID.setSelectedItem(listener.getOID());
            }

            //GET
            if(jComboBoxOperations.getSelectedItem().toString().equals("GET")) 
            {
                int[] n = OIDint();

                pdu.add(new VariableBinding(new OID(n)));
                pdu.setType(PDU.GET);

                Asynchronous(pdu);
                
                if(listener.getNoeud() == false)
                    showMessageDialog(this, "Vous ne pouvez pas interroger ce noeud directement", "Error", ERROR_MESSAGE);
            }

            //SET
            if(jComboBoxOperations.getSelectedItem().toString().equals("SET")) 
            {
                int[] n = OIDint();

                //Test du champ
                pdu.add(new VariableBinding(new OID(n)));
                pdu.setType(PDU.GET);

                Asynchronous(pdu);

                //SET si possible
                if(listener.getSet() == true)
                {
                    SNMP_SET sets = new SNMP_SET(this, true);
                    sets.setVisible(true);

                    if(sets.getClose() == true)
                    {
                        PDU pduset = new PDU();
                        pduset.add(new VariableBinding(new OID(n), new OctetString(sets.getValue())));
                        pduset.setType(PDU.SET);

                        Asynchronous(pduset);
                    }
                }
                else
                    showMessageDialog(this, "La mofication est impossible.", "Error", ERROR_MESSAGE); 
            }
        }   
        else
            showMessageDialog(this, "Veuiller encoder une adresse, une cummunity et un OID", "Error", ERROR_MESSAGE);
    }//GEN-LAST:event_jButtonGOActionPerformed

    private void jButtonConnexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConnexionActionPerformed
        boolean trouve;
        trouve = Ping();
        if(trouve == false)
            showMessageDialog(this, "L'adresse " + jTextFieldAddress.getText() + " repond.");
    }//GEN-LAST:event_jButtonConnexionActionPerformed

    private void jComboBoxOIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxOIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxOIDActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrameSNMP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrameSNMP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrameSNMP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrameSNMP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrameSNMP().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonConnexion;
    private javax.swing.JButton jButtonGO;
    private javax.swing.JComboBox<String> jComboBoxOID;
    private javax.swing.JComboBox<String> jComboBoxOperations;
    private javax.swing.JLabel jLabelAddress;
    private javax.swing.JLabel jLabelCommunity;
    private javax.swing.JLabel jLabelOID;
    private javax.swing.JLabel jLabelOperations;
    private javax.swing.JLabel jLabelPort;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableSNMP;
    private javax.swing.JTextField jTextFieldAddress;
    private javax.swing.JTextField jTextFieldCommunity;
    private javax.swing.JTextField jTextFieldPort;
    // End of variables declaration//GEN-END:variables
}
